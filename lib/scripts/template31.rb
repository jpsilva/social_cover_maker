 
module Template31
  def self.template31(input_array, output_path)

  rails_root = File.expand_path("../../..", __FILE__)


	profileBg 			= "#{rails_root}/lib/assets/templateFiles/commonFiles/Profile_back_facebook.jpg"
	coverBg 			= "#{rails_root}/lib/assets/templateFiles/commonFiles/Cover_back_facebook.jpg"

	peliculeCoverFile	= "#{rails_root}/lib/assets/templateFiles/template31/FTemplate31-Cover.png"
	#peliculeProfileFile	= "templateFiles/FTemplate01-Profile.png"

	output_profile		= output_path + "/profile.jpg"
	output_cover		= output_path + "/cover.jpg"



	# Load da imagem:
	imagemCover   		= MiniMagick::Image.open(input_array[0])
	imagemCover2   		= MiniMagick::Image.open(input_array[1])
	imagemProfile 		= MiniMagick::Image.open(input_array[2])
	profilebackground   = MiniMagick::Image.open(profileBg)
	coverbackground   	= MiniMagick::Image.open(coverBg)
	coverPelicule   	= MiniMagick::Image.open(peliculeCoverFile)
	#profilePelicule   	= MiniMagick::Image.open(peliculeProfileFile)

	# crop da parte pedida:
	height=314
	width=851
	pos_x=0
	pos_y=0

	# imagemProfile.crop 	 "#{width}x#{height}+#{pos_x}+#{pos_y}"
	# imagemProfile.resize "851x358!"

	imagemCover.crop 	 "#{width}x#{height}+#{pos_x}+#{pos_y}"
	# imagemCover.resize   "851x358!"

	## Gerar as duas imagens do Google+
	# Cortar e gerar a imagem Profile
	# imagemProfile.crop 	 "160x160!+28+198"
	# profilebackground  = profilebackground.composite(imagemProfile, "jpg") do |c|
	#   c.resize 			 "180x180!"
	#   c.gravity 		 "center"
	#   c.quality   	  	 "100"
	# end

	#profilebackground  = profilebackground.composite profilePelicule 
	imagemProfile.write output_profile


	##COVER
	#Cortar a grande, juntar a pequena e pôr película
	# imagemCover.crop 	 "851x314!+0+0"
	coverbackground    = coverbackground.composite(imagemCover, "jpg") do |c|
	  c.gravity 		 "center"
	  c.quality   		 "100"
	end

	#Gerar imagem pequena da cover
	imagemCover2.format "PNG32"
	imagemCover2 = imagemCover2.composite imagemCover2 do |c|
	     c.rotate "27.6"
	     c.background "transparent"
	end

	coverbackground = coverbackground.composite imagemCover2 do |c|
	    c.geometry "192!x176!+92+51"
	end

	coverbackground    = coverbackground.composite coverPelicule 
	coverbackground.write output_cover
  end
end