
module Template21


def self.template21(input_array, output_path)
  rails_root = File.expand_path("../../..", __FILE__)
  bg="#{rails_root}/lib/assets/templateFiles/commonFiles/Coverbackground_Original.png"
  coverbg="#{rails_root}/lib/assets/templateFiles/template21/Template21-Coverbackground.png"
  pbg="#{rails_root}/lib/assets/templateFiles/commonFiles/Profilebackground.jpg"
  profilebg="#{rails_root}/lib/assets/templateFiles/template21/Template21-Profilebackground.png"

  output_profile = output_path + "/profile.jpg"
  output_cover = output_path + "/cover.jpg"


  # Load da imagem:
  magick_input1 = MiniMagick::Image.open(input_array[0])
  magick_input2 = MiniMagick::Image.open(input_array[1])
  magick_input3 = MiniMagick::Image.open(input_array[2])
  magick_input4 = MiniMagick::Image.open(input_array[3])
  background   = MiniMagick::Image.open(bg)
  coverbackground   = MiniMagick::Image.open(coverbg)

  magick_Profile = MiniMagick::Image.open(input_array[4])
  pbackground   = MiniMagick::Image.open(pbg)
  profilebackground   = MiniMagick::Image.open(profilebg)

  ## Gerar a cover do Google+
  magick_input1 = magick_input1.composite magick_input1 do |c|
    c.background "transparent"
  end
  background = background.composite magick_input1 do |c|
    c.geometry "87x82!+86+38"
  end


  magick_input2 = magick_input2.composite magick_input2 do |c|
    c.rotate "-25"
    c.background "transparent"
  end
  background = background.composite magick_input2 do |c|
    c.geometry "115x114!+212+39"
  end


  magick_input3 = magick_input3.composite magick_input3 do |c|
    c.rotate "-15"
    c.background "transparent"
  end
  background = background.composite magick_input3 do |c|
    c.geometry "110x106!+352+36"
  end

  magick_input4 = magick_input4.composite magick_input4 do |c|
    c.rotate "15.5"
    c.background "transparent"
  end
  background = background.composite magick_input4 do |c|
    c.geometry "107x106!+472+34"
  end

  background = background.composite coverbackground
  background.format "jpeg"
  background.write output_cover

  ## Gerar a profile do Google+
  magick_Profile = magick_Profile.composite magick_Profile do |c|
    c.rotate "-4.5"
    c.background "transparent"
  end
  pbackground = pbackground.composite magick_Profile do |c|
    c.geometry "163x153!+78+88"
  end

  pbackground = pbackground.composite profilebackground
  pbackground.format "jpeg"
  pbackground.write output_profile
end

end