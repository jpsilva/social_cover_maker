#! /usr/bin/ruby -w
# Recebe 4 imagens e gera uma Cover com 3 imagens e duas mais pequenas
#  Fundo transparente
 
module Template30
  def self.template30(input_array, output_path)

    rails_root = File.expand_path("../../..", __FILE__)

    profileBg 			= "#{rails_root}/lib/assets/templateFiles/commonFiles/Profile_back_facebook.jpg"
    coverBg         = "#{rails_root}/lib/assets/templateFiles/commonFiles/Cover_back_facebook.jpg"

    peliculeCoverFile	= "#{rails_root}/lib/assets/templateFiles/template30/FTemplate30-Cover.png"

    #peliculeProfileFile	= "templateFiles/FTemplate01-Profile.png"

    output_profile = output_path + "/profile.jpg"
    output_cover = output_path + "/cover.jpg"

    # Load da imagem:
    imagemProfile 		= MiniMagick::Image.open(input_array[0])
    imagemCover   		= MiniMagick::Image.open(input_array[0])
    profilebackground   = MiniMagick::Image.open(profileBg)
    coverbackground   	= MiniMagick::Image.open(coverBg)
    coverPelicule   	= MiniMagick::Image.open(peliculeCoverFile)
    #profilePelicule   	= MiniMagick::Image.open(peliculeProfileFile)

    ## Gerar as duas imagens do Google+
    # Cortar e gerar a imagem Profile
    imagemProfile.crop 	 "160x160!+28+198"
    profilebackground  = profilebackground.composite(imagemProfile, "jpg") do |c|
      c.resize 			 "180x180!"
      c.gravity 		 "center"
      c.quality   	  	 "100"
    end

    #profilebackground  = profilebackground.composite profilePelicule 
    profilebackground.write output_profile

    # Cortar e gerar a imagem Cover
    imagemCover.crop 	 "851x314!+0+0"
    coverbackground    = coverbackground.composite(imagemCover, "jpg") do |c|
      c.gravity 		 "center"
      c.quality   		 "100"
    end

    coverbackground    = coverbackground.composite coverPelicule
    coverbackground.write output_cover

  end
end