# encoding: utf-8
# Recebe 3 imagens e gera uma  Cover com as frames do Cinema/fotografia e uma profile com alguma ruido

module Template33
  def self.template33(input_array, output_path)

    rails_root = File.expand_path("../../..", __FILE__)

  	profileBg 			= "#{rails_root}/lib/assets/templateFiles/commonFiles/Profile_back_facebook.jpg"
  	coverBg 			= "#{rails_root}/lib/assets/templateFiles/commonFiles/Cover_back_facebook.jpg"

  	postitBg   = "#{rails_root}/lib/assets/templateFiles/template33/FTemplate33-postit.png"
    peliculeCoverFile	= "#{rails_root}/lib/assets/templateFiles/template33/FTemplate33-Cover.png"

    #output_profile		= "output/profile.jpg"
  	output_cover		= output_path + "/cover.jpg"


    # Load da imagem:
    imagemCover1   		= MiniMagick::Image.open(input_array[2])
    imagemCover2   		= MiniMagick::Image.open(input_array[0])
    imagemCover3  		= MiniMagick::Image.open(input_array[1])
    #imagemProfile 		= MiniMagick::Image.open(input3)
    profilebackground   = MiniMagick::Image.open(profileBg)
    coverbackground   	= MiniMagick::Image.open(coverBg)
    postitbackground     = MiniMagick::Image.open(postitBg)
    coverPelicule   	= MiniMagick::Image.open(peliculeCoverFile)
    # profilePelicule   	= MiniMagick::Image.open(peliculeProfileFile)

    ## Gerar as duas imagens do Facebook
    # PROFILE
    # imagemProfile    	= imagemProfile.composite(profilePelicule, "jpg") do |c|
    #   c.gravity 		  "center"
    #   c.quality   		  "100"
    # end
    # imagemProfile.modulate "100,0,0"
    # imagemProfile.write   output_profile

    ##COVER
    # Cortar a grande, juntar a pequena e pôr película
    coverbackground.format "PNG32"
    coverbackground.background "transparent"


    imagemCover2.format     "PNG32"
    imagemCover2 = imagemCover2.composite imagemCover2 do |c|
      c.rotate     "3.3"
      c.background "transparent"
    end
    coverbackground 	= coverbackground.composite imagemCover2 do |c|
        c.geometry 		  "136!x175!+9+46"
    end

    imagemCover3.format     "PNG32"
    imagemCover3 = imagemCover3.composite imagemCover3 do |c|
      c.rotate     "25.3"
      c.background "transparent"
    end
    coverbackground 	= coverbackground.composite imagemCover3 do |c|
        c.geometry 		  "199!x178!+126+86"
    end

    imagemCover1.format     "PNG32"
    imagemCover1 = imagemCover1.composite imagemCover1 do |c|
      c.rotate     "-8.70"
      c.background "transparent"
    end
    coverbackground     = coverbackground.composite(imagemCover1) do |c|
      c.geometry          "177!x144!+177+5"
    end

    # Pelicula
    coverbackground     = coverbackground.composite coverPelicule 

    # Cria imagem, adiciona texto e roda a imagem

    postitbackground.combine_options do |c|
         c.font "#{rails_root}/lib/assets/templateFiles/template33/Template33-Font.ttf"
         c.pointsize '18'
         text = linner(input_array[3],20)
         c.draw "text 5,20 '#{text}'"
         c.fill("#000000")
      end
    postitbackground.format     "PNG32"
    postitbackground = postitbackground.composite postitbackground do |c|
      c.rotate   "4"
      c.background "transparent"
    end
    coverbackground   = coverbackground.composite postitbackground do |c|
        c.geometry      "x+510+78"
    end

    coverbackground.crop   "851x314+0+0"
    coverbackground.write output_cover

  end


  # Mete newLines quando é preciso
  def self.linner(text, cut_number)
    lineNumber = 0
    returnString = ""
    lines = text.split("\r\n")

    lines.each do | line |
      if line.length < cut_number
        returnString += line + "\n"
      else
        line.scan(/[\wÇçÁáÀà^ÂâÃãÉéÈèÊêÍíÌìÓóÒòÕõÔôÚúÙùÛûÜü\:\-\_\!\.\?\,\;]+/) do |w|
          #1. talvez seja preciso cortar palavras com tamanho demasiado grande
          #2. isto tem um problema quando encontra caracteres não ascii
          #3. convem ter um calculo se encontrar muitos M's ou muitos I's ou L's
          if (lineNumber + w.length) < cut_number
            returnString += w + " "
            lineNumber += w.length
          else
            returnString += "\n" + w + " "
            lineNumber = w.length
          end
        end
        returnString += "\n"
      end
    end

    return returnString

  end

end