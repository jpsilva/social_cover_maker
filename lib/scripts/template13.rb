module Template13

def self.template13(input_array, output_path)
   rails_root = File.expand_path("../../..", __FILE__)

   bg="#{rails_root}/lib/assets/templateFiles/template13/Coverbackground_Original.png"
   coverbg="#{rails_root}/lib/assets/templateFiles/template13/Template13-Coverbackground.png"
   pbg="#{rails_root}/lib/assets/templateFiles/template13/Profilebackground.jpg"
   profilebg="#{rails_root}/lib/assets/templateFiles/template13/Template13-Profilebackground.png"

   output_profile = output_path + "/profile.jpg"
   output_cover = output_path + "/cover.jpg"

   # Load da imagem:
   magick_input1 = MiniMagick::Image.open(input_array[0])
   magick_input2 = MiniMagick::Image.open(input_array[1])
   magick_input3 = MiniMagick::Image.open(input_array[2])
   magick_input4 = MiniMagick::Image.open(input_array[3])
   background   = MiniMagick::Image.open(bg)
   coverbackground   = MiniMagick::Image.open(coverbg)

   magick_Profile = MiniMagick::Image.open(input_array[4])   
   pbackground   = MiniMagick::Image.open(pbg)
   profilebackground   = MiniMagick::Image.open(profilebg)

   ## Gerar a cover do Google+
   magick_input1 = magick_input1.composite magick_input1 do |c|
     c.rotate "-10.4"
     c.background "transparent"
   end
   
   background = background.composite magick_input1 do |c|
     c.geometry "170x147!+60+20"
   end

   magick_input2 = magick_input2.composite magick_input2 do |c|
     c.rotate "3"
     c.background "transparent"
   end
   
   background = background.composite magick_input2 do |c|
     c.geometry "132x159!+225+10"
   end
   
   background = background.composite magick_input3 do |c|
     c.geometry "120x125!+365+44"
   end
   
   background = background.composite magick_input4 do |c|
     c.geometry "123x151!+489+14"
   end
   
   background = background.composite coverbackground 
   background.format "jpeg"
   background.write output_cover
   
   ## Gerar a profile do Google+
   pbackground = pbackground.composite magick_Profile do |c|
     c.geometry "211x210!+61+62"
   end
   
   pbackground = pbackground.composite profilebackground 
   pbackground.format "jpeg"
   pbackground.write output_profile
end
end