#! /usr/bin/ruby -w
# Recebe 4 imagens e gera uma Cover com 3 imagens e duas mais pequenas
#  Fundo transparente
 
#require 'rubygems'
#require 'mini_magick'

module Template27

def self.template27(input_array, output_path)
rails_root = File.expand_path("../../..", __FILE__)

profileBg="#{rails_root}/lib/assets/templateFiles/commonFiles/Profilebackground.jpg"
coverBg = "#{rails_root}/lib/assets/templateFiles/template27/Coverback.jpg"

peliculeCoverFile="#{rails_root}/lib/assets/templateFiles/template27/Template27-Coverbackground.png"
peliculeProfileFile="#{rails_root}/lib/assets/templateFiles/template27//Template27-Profilebackground.png"

output_profile = output_path + "/profile.jpg"
output_cover = output_path + "/cover.jpg"

height=250
width=940
pos_x=0
pos_y=0

# Load da imagem:
imagemProfile     = MiniMagick::Image.open(input_array[0])
imagemCover       = MiniMagick::Image.open(input_array[0])
profilebackground   = MiniMagick::Image.open(profileBg)
coverbackground      = MiniMagick::Image.open(coverBg)
coverPelicule   = MiniMagick::Image.open(peliculeCoverFile)
profilePelicule   = MiniMagick::Image.open(peliculeProfileFile)


# Verifica se o corte da imagem não excede os limites
if ((imagemProfile["height"]-height-pos_y)<0 or (imagemProfile["width"]-width-pos_x)<0)
   puts "ERRO: o corte da imagem excede os seus limites"
   return
end

# crop da parte pedida:
imagemProfile.crop "#{width}x#{height}+#{pos_x}+#{pos_y}"
imagemProfile.resize "940x250!"

imagemCover.crop "#{width}x#{height}+#{pos_x}+#{pos_y}"
imagemCover.resize "940x250!"

## Gerar as duas imagens do Google+
# Cortar e gerar a imagem Profile
imagemProfile.crop "250x250!+630+0"
profilebackground = profilebackground.composite(imagemProfile, "jpg") do |c|
  c.gravity "center"
  c.quality "100"
end

profilebackground = profilebackground.composite profilePelicule 
profilebackground.format "jpeg"
profilebackground.write output_profile

# Cortar e gerar a imagem Cover
imagemCover.crop "940x180!+0+39"
coverbackground = coverbackground.composite(imagemCover, "jpg") do |c|
  c.gravity "center"
  c.quality "100"
end

coverbackground = coverbackground.composite coverPelicule 
coverbackground.format "jpeg"
coverbackground.write output_cover

end

end




