module Template20
  
  def self.template20(input_array, output_path)
    rails_root = File.expand_path("../../..", __FILE__)

    coverbg= "#{rails_root}/lib/assets/templateFiles/template7/Coverback.jpg"
    profilebg= "#{rails_root}/lib/assets/templateFiles/template7/Profilebackground.jpg"
    watermarkfile= "#{rails_root}/lib/assets/templateFiles/commonFiles/WaterMark_SCM_transparent_base_sombras.png"

    peliculeCoverFile="#{rails_root}/lib/assets/templateFiles/template20/Template20-Coverbackground.png"
    peliculeProfileFile="#{rails_root}/lib/assets/templateFiles/template20/Template20-Profilebackground.png"

    output_profile = output_path + "/profile.jpg"
    output_cover = output_path + "/cover.jpg"


    # Load da imagem:
    imagemProfile = MiniMagick::Image.open(input_array[0])
    imagemCover = MiniMagick::Image.open(input_array[0])
    coverbackground = MiniMagick::Image.open(coverbg)
    profilebackground = MiniMagick::Image.open(profilebg)
    watermark = MiniMagick::Image.open(watermarkfile)

    coverPelicule   = MiniMagick::Image.open(peliculeCoverFile)
    profilePelicule   = MiniMagick::Image.open(peliculeProfileFile)



    ## Gerar as duas imagens do Google+
    # Cortar e gerar a imagem Profile
    imagemProfile.crop "250x250!+630+0"
    profilebackground = profilebackground.composite(imagemProfile, "jpg") do |c|
      c.gravity "center"
      #c.quality "100"
    end
    profilebackground = profilebackground.composite profilePelicule
    profilebackground.format "jpeg"
    profilebackground.write output_profile

    # Cortar e gerar a imagem Cover
    imagemCover.crop "940x180!+0+39"
    coverbackground = coverbackground.composite(imagemCover, "jpg") do |c|
      c.gravity "center"
      #c.quality "100"
    end

    #watermark
    coverbackground = coverbackground.composite(watermark, "jpg") do |c|
      c.geometry "250x180!+628+0"
    end

    coverbackground = coverbackground.composite coverPelicule
    coverbackkground.format "jpeg"
    coverbackground.write output_cover

  end

end