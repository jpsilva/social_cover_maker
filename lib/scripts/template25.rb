#! /usr/bin/ruby -w
# Recebe uma imagem e gera duas: uma Cover e uma Profile.
#   O resultado é ao juntar no Google+ parecer que as 
#   duas imagens são uma só.
 
#require 'rubygems'
#require 'mini_magick'

module Template25

def self.template25(input_array, output_path)
   rails_root = File.expand_path("../../..", __FILE__)

   bg="#{rails_root}/lib/assets/templateFiles/commonFiles/Coverbackground_Original.png"
   coverbg="#{rails_root}/lib/assets/templateFiles/template25/Template25-Coverbackground.png"

   pbg="#{rails_root}/lib/assets/templateFiles/commonFiles/Profilebackground.jpg"
   profilebg="#{rails_root}/lib/assets/templateFiles/template25/Template25-Profilebackground.png"


   output_profile = output_path + "/profile.jpg"
   output_cover = output_path + "/cover.jpg"


   # Load da imagem:
   magick_input1 = MiniMagick::Image.open(input_array[0])
   background   = MiniMagick::Image.open(bg)
   coverbackground   = MiniMagick::Image.open(coverbg)

   magick_Profile = MiniMagick::Image.open(input_array[1])
   pbackground   = MiniMagick::Image.open(pbg)
   profilebackground   = MiniMagick::Image.open(profilebg)

   ## Gerar a cover do Google+
   magick_input1 = magick_input1.composite magick_input1 do |c|
     c.background "transparent"
   end
   
   background = background.composite magick_input1 do |c|
      c.geometry "613x178!+10+1"
   end

   background = background.composite coverbackground 
   background.format "jpeg"
   background.write output_cover

   ## Gerar a profile do Google+
   pbackground = pbackground.composite magick_Profile do |c|
     c.geometry "241x249!+52+42"
   end
   
   pbackground = pbackground.composite profilebackground 
   pbackground.format "jpeg"
   pbackground.write output_profile

end

end


