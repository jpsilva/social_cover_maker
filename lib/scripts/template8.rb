#! /usr/bin/ruby -w
# Recebe uma imagem e gera duas: uma Cover e uma Profile.
#   O resultado é ao juntar no Google+ parecer que as 
#   duas imagens são uma só.
#
# Dependências: ttf-Droid
 
#require 'rubygems'
#require 'mini_magick'

module Template8


def self.template8(input_array, output_path)

  rails_root = File.expand_path("../../..", __FILE__)
   coverbg="#{rails_root}/lib/assets/templateFiles/template8/Template8-Coverbackground.png"
   output_profile = output_path + "/profile.jpg"
  output_cover = output_path + "/cover.jpg"


   # Load da imagem:
   magick_input1 = MiniMagick::Image.open(input_array[0])
   magick_input2 = MiniMagick::Image.open(input_array[1])
   magick_input3 = MiniMagick::Image.open(input_array[2])
   magick_input4 = MiniMagick::Image.open(input_array[3])
   coverbackground   = MiniMagick::Image.open(coverbg)
   #profilebackground   = MiniMagick::Image.open(profilebg)
#   coverbackground.combine_options do |c|
#     c.quality "100"
#   end

   ## Gerar a cover do Google+
   coverbackground = coverbackground.composite(magick_input1, "jpg") do |c|
     c.geometry "170x170!+5+5"
     c.quality "100"
   end
   
   coverbackground = coverbackground.composite(magick_input2, "jpg") do |c|
     c.geometry "131x131!+190+44"
     c.quality "100"
   end
   
   coverbackground = coverbackground.composite(magick_input3, "jpg") do |c|
     c.geometry "131x131!+336+44"
     c.quality "100"
   end
   
   coverbackground = coverbackground.composite(magick_input4, "jpg") do |c|
     c.geometry "131x131!+482+44"
     c.quality "100"
   end
   
   coverbackground.combine_options do |c|
      c.font "/usr/share/fonts/truetype/Arial.ttf"
      c.pointsize '26'
      c.draw "text 184,30 '#{input_array[4]}'"
      c.fill("#000000")
   end 
   
   coverbackground.format "jpeg"
   coverbackground.write output_cover



end

end



