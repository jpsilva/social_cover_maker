#! /usr/bin/ruby -w
# Racios das imagens:
# imagem1: 1.3125
# imagem2: 1.327
# imagem3: 1.157
# imagemProfile: 1.232


#require 'rubygems'
#require 'mini_magick'

module Template18


def self.template18(input_array, output_path)

  rails_root = File.expand_path("../../..", __FILE__)
  cover_background_file   ="#{rails_root}/lib/assets/templateFiles/commonFiles/Coverbackground_Original.png"
  cover_pelicule_file     ="#{rails_root}/lib/assets/templateFiles/template18/Template18-Coverbackground.png"
  profile_background_file ="#{rails_root}/lib/assets/templateFiles/commonFiles/Profilebackground.jpg"
  profile_pelicule_file   ="#{rails_root}/lib/assets/templateFiles/template18/Template18-Profilebackground.png"
  output_profile          =output_path + "/profile.jpg"
  output_cover            =output_path + "/cover.jpg"


  # Load da imagem:
  image1             = MiniMagick::Image.open(input_array[0])
  image2             = MiniMagick::Image.open(input_array[1])
  image3             = MiniMagick::Image.open(input_array[2])
  cover_background   = MiniMagick::Image.open(cover_background_file)
  cover_pelicule     = MiniMagick::Image.open(cover_pelicule_file)

  imageProfile       = MiniMagick::Image.open(input_array[3])
  profile_background = MiniMagick::Image.open(profile_background_file)
  profile_pelicule   = MiniMagick::Image.open(profile_pelicule_file)

  ## Gerar a cover do Google+
  image2           = image2.composite image2 do |c|
    c.rotate "10.11"
    c.background "transparent"
  end

  cover_background = cover_background.composite image2 do |c|
    c.geometry "156x130!+261+6"
  end

  cover_background = cover_background.composite image3 do |c|
    c.geometry "140x121!+462+13"
  end

  cover_background = cover_background.composite image1 do |c|
    c.geometry "147x112!+29+16"
  end

  cover_background = cover_background.composite cover_pelicule
  cover_background.format "jpeg"
  cover_background.write output_cover

  ## Gerar a profile do Google+
  profile_background = profile_background.composite imageProfile do |c|
    c.geometry "250x203!+42+42"
  end
   
  profile_background = profile_background.composite profile_pelicule 
  profile_background.format "jpeg"
  profile_background.write output_profile
end
end


