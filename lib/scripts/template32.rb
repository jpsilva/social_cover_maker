# Recebe 3 imagens e gera uma Cover com as frames do Cinema/fotografia e uma profile com alguma ruido
module Template32
  def self.template32(input_array, output_path)

    rails_root = File.expand_path("../../..", __FILE__)

  	profileBg 			= "#{rails_root}/lib/assets/templateFiles/commonFiles/Profile_back_facebook.jpg"
  	coverBg 			= "#{rails_root}/lib/assets/templateFiles/commonFiles/Cover_back_facebook.jpg"

  	peliculeCoverFile	= "#{rails_root}/lib/assets/templateFiles/template32/FTemplate32-Cover.png"
  	peliculeProfileFile	= "#{rails_root}/lib/assets/templateFiles/template32/FTemplate32-Cover.png"

  	output_profile		= output_path + "/profile.jpg"
  	output_cover		= output_path + "/cover.jpg"

    # Load da imagem:
    imagemCover   		= MiniMagick::Image.open(input_array[0])
    imagemCover2   		= MiniMagick::Image.open(input_array[1])
    imagemCover3  		= MiniMagick::Image.open(input_array[1])
    imagemProfile 		= MiniMagick::Image.open(input_array[2])
    profilebackground   = MiniMagick::Image.open(profileBg)
    coverbackground   	= MiniMagick::Image.open(coverBg)
    coverPelicule   	= MiniMagick::Image.open(peliculeCoverFile)
    profilePelicule   	= MiniMagick::Image.open(peliculeProfileFile)

    ## Gerar as duas imagens do Facebook
    # PROFILE
    imagemProfile    	= imagemProfile.composite(profilePelicule, "jpg") do |c|
      c.gravity 		  "center"
      c.quality   		  "100"
    end
    imagemProfile.modulate "100,0,0"
    imagemProfile.write   output_profile

    ##COVER
    # Cortar a grande, juntar a pequena e pôr película
    coverbackground    	= coverbackground.composite(imagemCover, "jpg") do |c|
      c.geometry 		  "475!x253!+295+39"
    end

    #Gerar imagem parcial da fita do cinema
    #imagemCover3.crop 	 "180x16+180+164"
    imagemCover3.modulate "100,0,0"
    coverbackground 	= coverbackground.composite imagemCover3 do |c|
        c.geometry 		  "169!x15!+24+0"
    end

    #Gerar segunda imagem da fita do cinema
    imagemCover2.modulate "100,0,0"
    coverbackground 	= coverbackground.composite imagemCover2 do |c|
        c.geometry 		  "169!x162!+24+24"
    end

    #Gerar parcialmente a terceira imagem da fita do cinema
    coverbackground 	= coverbackground.composite imagemProfile do |c|
        c.geometry 		  "180!x180!+24+194"
    end

    coverbackground    	= coverbackground.composite coverPelicule
    coverbackground.write output_cover

  end
end