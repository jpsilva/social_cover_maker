#! /usr/bin/ruby -w
# Recebe uma imagem e gera duas: uma Cover e uma Profile.
#   O resultado é ao juntar no Google+ parecer que as 
#   duas imagens são uma só.
 
#require 'rubygems'
#require 'mini_magick'

module Template17

def self.template17(input_array, output_path)
   rails_root = File.expand_path("../../..", __FILE__)
   bg="#{rails_root}/lib/assets/templateFiles/commonFiles/Coverbackground_Original.png"
   coverbg="#{rails_root}/lib/assets/templateFiles/template17/Template17-Coverbackground.png"
   output_cover= output_path + "/cover.jpg"

   # Load da imagem:
   magick_input1 = MiniMagick::Image.open(input_array[0])
   background   = MiniMagick::Image.open(bg)
   coverbackground   = MiniMagick::Image.open(coverbg)

   ## Gerar a cover do Google+
   magick_input1 = magick_input1.composite magick_input1 do |c|
     c.background "transparent"
   end
   
   background = background.composite magick_input1 do |c|
      c.geometry "491x139!+57+13"
   end

   background = background.composite coverbackground 
   background.format "jpeg"
   background.write output_cover
end

end


