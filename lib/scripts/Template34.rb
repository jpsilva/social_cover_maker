# encoding: utf-8
#! /usr/bin/ruby -w
# Recebe 3 imagens e gera uma  Cover com as frames do Cinema/fotografia e uma profile com alguma ruido
 
module Template34
  def self.template34(input_array, output_path)

    rails_root = File.expand_path("../../..", __FILE__)
  	coverBg 			= "#{rails_root}/lib/assets/templateFiles/commonFiles/Cover_back_facebook.jpg"

    peliculeCoverFile	= "#{rails_root}/lib/assets/templateFiles/template34/FTemplate34-Cover.png"

    output_cover		= output_path + "/cover.jpg"

    # Load da imagem:
    imagemCover1   		= MiniMagick::Image.open(input_array[0])
    imagemCover2   		= MiniMagick::Image.open(input_array[1])
    imagemCover3  		= MiniMagick::Image.open(input_array[2])
    imagemCover4      = MiniMagick::Image.open(input_array[3])
    imagemCover5      = MiniMagick::Image.open(input_array[4])
    imagemCover6      = MiniMagick::Image.open(input_array[5])
    imagemCover7      = MiniMagick::Image.open(input_array[6])

    coverbackground   	= MiniMagick::Image.open(coverBg)
    coverPelicule   	= MiniMagick::Image.open(peliculeCoverFile)

    ##COVER
    # Cortar a grande, juntar a pequena e pôr película
    coverbackground 	= geometry(coverbackground,imagemCover1,"+8+10")
    coverbackground   = geometry(coverbackground,imagemCover2,"+204+10")
    coverbackground   = geometry(coverbackground,imagemCover3,"+204+164")
    coverbackground   = geometry(coverbackground,imagemCover4,"+360+164")
    coverbackground   = geometry(coverbackground,imagemCover5,"+516+10")
    coverbackground   = geometry(coverbackground,imagemCover6,"+700+10")
    coverbackground   = geometry(coverbackground,imagemCover7,"+700+164")


    # Pelicula
    coverbackground     = coverbackground.composite coverPelicule
    # Output:
    coverbackground.format "PNG"
    coverbackground.write output_cover

  end

  # Função que posiciona num X local:
  def self.geometry(cover,img, pos)
    cover   = cover.composite img do |c|
      c.geometry      "x#{pos}"
    end
    return cover
  end

end