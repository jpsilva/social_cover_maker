module IENoMore

def smack_down_ie6
	render :partial => "home/ie_upgrade_message" if is_ie6?
end

def is_ie6?
    request.env["HTTP_USER_AGENT"] =~ /MSIE 6+?/
end
end