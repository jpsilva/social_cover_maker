SocialCoverMaker::Application.routes.draw do

  ActiveAdmin.routes(self)

  devise_for :admin_users, ActiveAdmin::Devise.config

  scope "(:locale)", :locale => /#{I18n.available_locales.join("|")}/ do
    root :to => 'home#index'

    #acedidas a partir da root
    match '/about' => 'home#about'
    match '/privacy_policy' => 'home#privacy_policy', :as => 'privacy_policy'
    match '/about_us' => 'home#about_us'
    match '/features' => 'home#features'
    match '/faq' => 'home#faq', :as => 'faq'
    #contact us
    match 'contact' => 'contact#new', :as => 'contact', :via => :get
    match 'contact' => 'contact#create', :as => 'contact', :via => :post
    match 'contact_success' => 'contact#contact_success', :via => :get
    match '/youtube_lightbox' => 'home#youtube_lightbox'


    get "blog_comments/index"
    resources :blog_comments do
      get 'n_comments', :on => :collection
    end

    controller :blog_entries, :only => [:index, :create] do
      match '/blog' => :index, :as => :blog ,:via => [:get]
      match '/blog/:slug' => :show, :as => :blog_entry, :via => [:get]
      match '/blog/tag/:slug' => :index_by_tag, :as => :blog_index_by_tag, :via => [:get]
      match '/blog/category/:slug' => :index_by_category, :as => :blog_index_by_category, :via => [:get]
    end

    #templates
    resources :templates, :only => [:show, :index], :path => "themes" do
      post 'submit', :on => :collection
      post 'download', :on => :member
      get 'tab_comments', :on => :member
      get 'tab_items', :on => :member
      get 'tab_output', :on => :member
      get 'request_download_links', :on => :member
      get 'tab_similars', :on => :member
      get 'examples', :on => :member
      post 'submit_template_example', :on => :collection
    end
#    match '/templates/tag/:slug' => :index_by_tag, :as => :templates_index_by_tag, :via => [:get], :controller => :templates
#    match '/templates/category/:slug' => :index_by_category, :as => :templates_index_by_category, :via => [:get], :controller => :templates

    resources :template_examples, :only => [:index]

    resources :template_comments, :only => [:index, :create] do
      get 'n_comments', :on => :collection
    end

    resources :images
    match '/images/:id/crop' => 'images#crop'

    resources :template_categories
    resources :blog_categories

    match "/404", :to => "errors#not_found"#adicionada para que a pagina 404 seja tratada pelo controlador
  end

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
