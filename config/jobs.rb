require "mysql2"


Dir[File.expand_path("../../lib/scripts/*.rb", __FILE__)].each {|file| require file }
require 'mini_magick'

RAILS_ENV = ENV["RAILS_ENV"] || "development"

con = Mysql2::Client.new(:host => "localhost", :username => "root", :password => "12345", :database => "cover_maker_development", :reconnect => true)

job "template.process" do |args|

  #  con.close()
  #if !con.ping()
  #  con = Mysql2::Client.new(:host => "localhost", :username => "scmdb", :password => "", :database => "cover_maker_production")
  #end

  script_params = args["params"]
  output_path = args["output_folder"]

  case args["template_id"]
  when 1
    Template7.template7(script_params, output_path)
  when 2
    Template8.template8(script_params, output_path)
  when 3
    Template9.template9(script_params, output_path)
  when 4
      Template10.template10(script_params, output_path)
  when 5
      Template11.template11(script_params, output_path)
  when 6
      Template12.template12(script_params, output_path)
  when 7
    Template13.template13(script_params, output_path)
  when 8
    Template14.template14(script_params, output_path)
  when 9
    Template27.template27(script_params, output_path)
  when 10
    Template17.template17(script_params, output_path)
  when 11
    Template18.template18(script_params, output_path)
  when 12
    Template20.template20(script_params, output_path)
  when 13
    Template21.template21(script_params, output_path)
  when 14
    Template22.template22(script_params, output_path)
  when 15
    Template23.template23(script_params, output_path)
  when 16
    Template24.template24(script_params, output_path)
  when 17
    Template25.template25(script_params, output_path)
  when 18
    Template26.template26(script_params, output_path)
  when 19
    Template28.template28(script_params, output_path)
  when 20
    Template29.template29(script_params, output_path)
  when 21
    Template30.template30(script_params, output_path)
  when 22
    Template31.template31(script_params, output_path)
  when 23
    Template32.template32(script_params, output_path)
  when 24
    Template33.template33(script_params, output_path)
  when 25
    Template34.template34(script_params, output_path)
  else
    Template9.template9(script_params, output_path)
  end

  con.query("update image_processing_jobs set finished=1, success=1 where id=#{args['imgPJ_id']}")
  FileUtils.chmod_R 0775, output_path#dar permissoes para ler a imagem
end

error do |exception, job, args|
  print "EXCEPTION: " + exception.to_s
  con.query("update image_processing_jobs set finished=1, success=0 where id=#{args['imgPJ_id']}")
end


=begin
require File.expand_path("../environment", __FILE__)

job "template.process" do |args|
  print "\n\n\nCENAS\n\n\n"
  Template.exec_script(args["template_id"], args["params"], args["output_folder"])
  imgPJ = ImageProcessingJob.find(args["imgPJ_id"])
  imgPJ.finished = true
  imgPJ.success = true
  imgPJ.save
end

error do |exception, job, args|  print "\n"
  print "EXCEPTION: " + exception.to_s
  imgPJ = ImageProcessingJob.find(args["imgPJ_id"])
  imgPJ.finished = true
  imgPJ.success = false
  imgPJ.save
end
=end