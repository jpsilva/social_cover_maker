class TemplateExamplesController < ApplicationController

  def index
    @offset = params[:offset].to_i
    @template = Template.find_by_slug(params[:slug])
    @example = @template.template_examples.limit(1).offset(@offset).first
    @total_examples = @template.template_examples.size
    @slug = params[:slug]
    @locale= params[:locale] || I18n.locale
    @website_name = @template.website.name

    render :layout => false
  end
end
