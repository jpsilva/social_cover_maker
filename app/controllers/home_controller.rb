class HomeController < ApplicationController
  include IENoMore

  before_filter :is_home_section, :only => [:index]
  before_filter :is_features_section, :only => [:features]
  before_filter :is_about_section, :only => [:about, :about_us, :contacts]
  before_filter :is_about_us_section, :only => [:about_us]
  before_filter :is_faq_section, :only => [:faq]

  def index
    @examples =  Template.get_top_six_templates
    print "\n\n\n\n---> " + @examples.size.to_s
    print "\n\n" + @examples.to_s
    @locale = params[:locale]
    @random_template = Template.get_random_template

    if !params[:locale]
      redirect_to root_path
    end
  end

  def about
    @title = t('about')
  end

  def about_us
    @title = t('team')
  end

  def contacts
    @title = t('contacts')
  end


  def privacy_policy
    @title = t('privacy_policy')
  end

  def features
    @title = t('features')
  end

  def youtube_lightbox
    @link = params[:link]
    render :layout => false
  end
  
  def faq
    
  end

  private
  
  #permite definir a secção página inicial como selecionada na barra superior
  def is_home_section
      @is_home_section = 1;
    end
  
  #permite definir a secção funcionalidades como selecionada na barra superior
  def is_features_section
    @is_features_section = 1;
  end
  
  #permite definir a secção about como selecionada na barra superior
  def is_about_section
    @is_about_section = 1;
  end
  
  def is_about_us_section
    @is_about_us_section = 1;
  end

  def is_ie6
    request.env["HTTP_USER_AGENT"] =~ /MSIE 6+?/
  end
  
  def is_faq_section
    @is_faq_section = 1;
  end

end