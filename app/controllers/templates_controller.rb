class TemplatesController < ApplicationController
  before_filter :templates_per_page, :only => [:index, :index_by_tag, :index_by_category]
  before_filter :prepare_menu_data, :only => [:index, :show, :index_by_tag, :index_by_category]
  before_filter :is_templates_section, :only => [:index, :show, :index_by_tag, :index_by_category]
  
  def index
   #@templates = Template.where('1 = 1').paginate(:per_page => 2, :page => params[:page])
   #for title and meta datas
   if !params[:facebook] 
      @google_active = 1
   end
   if !params[:google]
      @facebook_active = 1
   end


   @locale = params[:locale]
   @order_by = params[:order_by] || "Date"
   @period_of_time = params[:period_of_time] || "week"

   if params[:tag] && tag = Tag.where(:slug => params[:tag]).first
     @templates = tag.templates
     @tag_slug = tag.slug
     @title = tag.name

  elsif params[:category] && @category = TemplateCategory.where(:slug => params[:category]).first
    @templates = @category.templates
    @category_slug = @category.slug
    @title = @category.name
    @is_category_search = 1
  else
    @title = t('templates')
    @templates = Template
  end

  if (params[:order_by])
     case params[:order_by]
       when "date"
         @templates = Template.get_templates_by_date.paginate(@templates, :per_page => templates_per_page, :page => params[:page])
       else
         @templates = Template.get_templates_by_views_or_executions(@templates, @period_of_time, @order_by).paginate(:per_page => templates_per_page, :page => params[:page])
     end
   else
     @templates = @templates.paginate(:per_page => templates_per_page, :page => params[:page])
   end

   if params[:google] and !params[:google].empty?
    @google_active = params[:google]
    @templates_google = Template.where(:website_id => '1')
  end

  if params[:facebook] and !params[:facebook].empty?
    @facebook_active = params[:facebook]
    @templates_face = Template.where(:website_id => '2')

  end
  
  if params[:google] and !params[:google].empty? and params[:facebook] and !params[:facebook].empty?
    @templates = @templates.where('templates.id in (?) or templates.id in(?)', @templates_google, @templates_face )
  else
    if params[:google] and !params[:google].empty?
      @templates = @templates.where('templates.id in (?)', @templates_google)
    end
    
    if params[:facebook] and !params[:facebook].empty?
      @templates = @templates.where('templates.id in (?)', @templates_face)
    end
  end
   render :index
  end

  def show
    @template = Template.find_by_slug(params[:id])
    @items = @template.items
    @image = Image.new
    @title =  t(@template.key + '-title')
    @locale = params[:locale]
    @ad = @template.ad

    @size_of_items_of_image_type = 0
    @items.each do |item|
      if item.item_type.name == "Imagem"
        @size_of_items_of_image_type += 1
      end
    end

    #tags with coma
    @len = @template.tags.count
    @count = 0

    #set views counts
    Counter.update_counter(@template.id, 'Views')

    #for title and meta datas
    @website_name = @template.website.name
    @meta_keywords = TagsTemplate.get_tags_keywords(@template)
    @meta_description = t(@template.key + '-desc').truncate(200)

    ############## Examples
    @offset = 0;
    @example = @template.template_examples.limit(1).offset(@offset).first
    @total_examples = @template.template_examples.size
    ###########################

    #ATT isto já nao deve ser necessário devido ás tabs
    ############Comments
=begin    @comments_count = TemplateComment.where(:template_id => @template.id, :approved => true).count
    @quantity = 2
    all_template_comments = @template.template_comments
    @all_template_comments_size = all_template_comments.where(:approved=>true).size
    @template_comments = all_template_comments.where(:approved=>true).limit(@quantity)
    @offset = 0
    @new_comment = TemplateComment.new
    ############
=end
    #tab_outputs
    session_id = request.session_options[:id]
    @imgPJ = ImageProcessingJob.where(:session_id => session_id,
                                      :template_id => @template.id, :finished => true, :success => true ).last

    #organizacao dos items na pagina
    @extend_first_div_item = false
    if @items.size.odd?
      @extend_first_div_item = true
    end
  end

  def submit
    @template = Template.find(params[:id])
    item_ids = @template.items.map(&:id)
    session_id = request.session_options[:id]    #########    #########    #########    #########
    #session_id = (1900 + rand(2000)).to_s
    @still_processing = false
    @session_expired = false

    #update execution counter
    Counter.update_counter(@template.id, 'Executions')

    script_param = Array.new
    item_ids.each do |id|

      #vai buscar o item e tipo do item
      item = Item.find_by_id(id)
      item_type = item.item_type

      #se for uma imagem adiciona o url
      if item_type.name == 'Imagem'
        image = Image.find_by_id(params[("hidden_" + id.to_s)])

        #se a imagem submetida já não existir, sair e retornar erro
        if !image
          @session_expired = true
          #render :js => template_path(:id => @template.slug)
           render 'submit.js.erb'
          return
        end

        script_param.push(File.join(Rails.root, 'public'+image.file_url(:cropped)))
      end

      #se for texto adiciona o proprio texto
      if item_type.name == 'Texto'
        script_param.push(params[("hidden_" + id.to_s)])
      end
      #print "ITEM ID: " +  id.to_s  + "IMAGE ID: " + params[("hidden_" + id.to_s).to_sym 
      #print "\n"
    end

    #Executa script para gerar o template
    if (@imgPJ = ImageProcessingJob.find_all_by_session_id_and_template_id(session_id, @template.id).last)
      if @imgPJ.finished == false
        @still_processing = true
      else
        #@imgPJ.update_attributes(:finished => false, :success => false)
        @imgPJ = @template.image_processing_jobs.create(:session_id => session_id )
      end
    else
      @imgPJ = @template.image_processing_jobs.create(:session_id => session_id )
    end

    output_path = File.join(Rails.root, 'public/downloads/' + @imgPJ.id.to_s)
    if !File.exist?(output_path)
      #Dir.mkdir(output_path)
      FileUtils.mkdir_p output_path
    end

    @beanstalkd_off = false
    begin
      Stalker.enqueue "template.process", {:template_id => @template.id, :params => script_param,
         :output_folder => output_path, :imgPJ_id => @imgPJ.id}#, {:ttr => 25, :pri => @template.label}
   rescue Exception => e
      @beanstalkd_off = true
    end

   render 'submit.js.erb'

  end

  def download
    #ID da sessão onde estão alojadas as imagens geradas pelos scripts
    if params[:download_cover]
      send_file "public/downloads/" + params[:imgPJ_id] +  "/cover.jpg", :type=> "jpg", :x_sendfile => true
    elsif params[:download_profile]
      send_file "public/downloads/" + params[:imgPJ_id] + "/profile.jpg", :type=> "jpg", :x_sendfile => true
    end

  end

  def submit_template_example
    imgpj = ImageProcessingJob.find(params[:imgPJ_id])
    template = Template.find(imgpj.template_id)
   
    #criar objecto do exemplo
      user_example = UserTemplateExample.create(:email => params[:email], :link => params[:link], :template_id => template.id)

      #criar pasta destino
      path = File.join(Rails.root, 'public/user_submitted_examples/template' +  template.id.to_s + '/' + user_example.id.to_s)
      if !File.exist?(path)
        FileUtils.mkdir_p path
      end

      #copiar para pasta
      src = File.join(Rails.root, 'public/downloads/' + imgpj.id.to_s + '/')
      FileUtils.cp(src + 'cover.jpg', path)
      if template.output == 2
        FileUtils.cp(src + 'profile.jpg', path)
      end

      redirect_to :back
      #render 'user_example_submitted.js.erb'
  end

=begin
  def index_by_tag
    @order_by = params[:order_by] || "Date"
    @period_of_time = params[:period_of_time] || "week"

    @templates = []
    tag = Tag.where(:slug => params[:slug]).first

    if tag
      @templates = tag.templates.paginate(:per_page => templates_per_page, :page => params[:page])

      #page title
      @title = tag.name
    end

    render :index
  end

  def index_by_category
    @order_by = params[:order_by] || "Date"
    @period_of_time = params[:period_of_time] || "week"

    @templates = []
    @category = TemplateCategory.where(:slug => params[:slug]).first
    if @category
        @templates = Template.where(:template_category_id => @category.id).paginate(:per_page => templates_per_page, :page => params[:page])
        @is_category_search = 1;

        #page title
        @title = @category.name
    end
    render :index
  end
=end

  def tab_comments
    @template = Template.find(params[:id])
    @comments_count = TemplateComment.where(:template_id => @template.id, :approved => true).count
    @quantity = 2
    all_template_comments = @template.template_comments
    @all_template_comments_size = all_template_comments.where(:approved=>true).size
    @template_comments = all_template_comments.where(:approved=>true).limit(@quantity)
    @offset = 0
    @new_comment = TemplateComment.new
    render 'tab_comments.js.erb'
  end

  def tab_items
    @template = Template.find(params[:id])
    @items = @template.items
    @image = Image.new
    @title = @template.name

    @size_of_items_of_image_type = 0
    @items.each do |item|
      if item.item_type.name == "Imagem"
        @size_of_items_of_image_type += 1
      end
    end

    #organizacao dos items na pagina
    @extend_first_div_item = false
    if @items.size.odd?
      @extend_first_div_item = true
    end

    render 'tab_items.js.erb'
  end

  def request_download_links
    @template = Template.find(params[:id])
    @imgPJ = ImageProcessingJob.find(params[:imgPJ_id])
    @website = @template.website.name
    render 'submit.js.erb'
  end

  def tab_output
    @template = Template.find(params[:id])

    @website = @template.website.name
    @imgPJ = ImageProcessingJob.where(:session_id => request.session_options[:id],
                                      :template_id => @template.id, :finished => true, :success => true ).last
    render 'tab_output.js.erb'
  end

  def tab_similars
    @similar_templates = true
    @locale = params[:locale]

    if !params[:facebook]
       @google_active = 1
    end
    if !params[:google]
       @facebook_active = 1
    end

    @order_by = params[:order_by] || "Date"
    @period_of_time = params[:period_of_time] || "week"

    template = Template.find_by_id(params[:id])
    @templates = Template.get_similars(template.id)
    if @templates
      @templates = @templates.paginate(:per_page => templates_per_page_similars, :page => params[:page])
    end

    render 'tab_similars.js.erb'
  end

  def examples
    @offset = params[:offset].to_i
    @locale = params[:locale]

    @template = Template.find(params[:id])
    @example = @template.template_examples.limit(1).offset(@offset).first
    print "\n\n\n" + @template.id.to_s + "\n\n\n"
    @website_name = @template.website.name

    @total_examples = @template.template_examples.size

    render 'examples.js.erb'
  end

  private

    def templates_per_page
      @templates_per_page = 6
    end

    def templates_per_page_similars
      @templates_per_page = 40;
    end

    def prepare_menu_data
      @categories = TemplateCategory.all
      @templates_by_year = Template.templates_by_year
    end

    #permite definir a secção template como selecionada na barra superior
    def is_templates_section
      @is_templates_section = 1;
    end
end
