class ErrorsController < ApplicationController
  def not_found
    @title = "Page not found (404)"
    counter_type = CounterType.where(:name => 'executions').first
    top_by_month = counter_type.counters.order('month DESC').limit(2)

    @top_templates = []
    top_by_month.each do |counter|
      @top_templates.push( Template.find(counter.template_id) )
    end
  end
end