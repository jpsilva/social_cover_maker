class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :set_locale, :meta_defaults, :populate_locale_options

  def set_locale
    logger.debug "* Accept-Language: #{request.env['HTTP_ACCEPT_LANGUAGE']}"
    I18n.locale = params[:locale] || extract_locale_from_accept_language_header || I18n.default_locale
    logger.debug "* Locale set to '#{I18n.locale}'"
  end

  def default_url_options(options={})
    logger.debug "default_url_options is passed options: #{options.inspect}\n"
    { :locale => I18n.locale }
  end

  def populate_locale_options
    @options = []
    I18n.available_locales.select {|locale| @options.push ([locale, url_for( :locale => locale )]) }
    @default = url_for(:locale => I18n.locale)
  end

  private
    def extract_locale_from_accept_language_header
      locale = nil
      if request.env['HTTP_ACCEPT_LANGUAGE']
        accepted_locale = request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first
        if ["en", "pt"].include? accepted_locale
          locale = accepted_locale
        end
      end
      locale
    end

    def meta_defaults
      #@meta_title = "Welcome to"
      @meta_keywords = "Google, Facebook, Google plus, Cover, profile, image, edit, crop, theme"
      @meta_description = t("meta_description")
    end
end
