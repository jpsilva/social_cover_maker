class TemplateCommentsController < ApplicationController
  before_filter :is_templates_section, :only => [:index]
  def index
    @template = Template.find_by_slug(params[:slug])
    @template_comments = @template.template_comments.where(:approved => true)
  end

  def n_comments
    @quantity = 2
    @template = Template.find(params[:template_id])
    all_template_comments = @template.template_comments
    @all_template_comments_size = all_template_comments.where(:approved=>true).size
    @template_comments = all_template_comments.where(:approved=>true).limit(@quantity).offset(params[:offset].to_i)
    @offset = params[:offset].to_i

    render 'n_comments.js.erb'
  end

  def create
    @template_comment = TemplateComment.new(params[:template_comment])
    @saved = false
    @saved =  @template_comment.save

    respond_to do | format |
      format.js {render :layout => false}
    end
  end

  def is_templates_section
    @is_templates_section = 1;
  end
end
