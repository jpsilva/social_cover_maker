class BlogCommentsController < ApplicationController
  before_filter :is_blog_section
  
  def index
    #@blog_entry = BlogEntry.find_by_slug(params[:slug])
    #@blog_comments = BlogComment.where('blog_entry_id = ?', @blog_entry.id)
    @blog_entry = BlogEntry.find_by_slug(params[:slug])
    @comments_count = BlogComment.where(:blog_entry_id => @blog_entry.id, :approved => true).count
    @quantity = 10
    all_blog_entry_comments = @blog_entry.blog_comments
    @all_blog_entry_comments_size = all_blog_entry_comments.where(:approved=>true).size
    @blog_comments = all_blog_entry_comments.where(:approved=>true).limit(@quantity)
    @offset = 0
    @new_comment = BlogComment.new
  end

  def n_comments
    if params[:full_page]
      @quantity = 10
      @full_page = true
    else
      @quantity = 2
      @full_page = false
    end

    @blog_entry = BlogEntry.find(params[:blog_entry_id])
    all_blog_entry_comments = @blog_entry.blog_comments
    @all_blog_entry_comments_size = all_blog_entry_comments.where(:approved=>true).size
    @blog_comments = all_blog_entry_comments.where(:approved=>true).limit(@quantity).offset(params[:offset].to_i)
    @offset = params[:offset].to_i

    render 'n_comments.js.erb'
#    respond_to do | format |
#      format.js {render :layout => false}
#    end
  end

  def create
    @blog_comment = BlogComment.new(params[:blog_comment])
    @saved = false
    @saved =  @blog_comment.save

    respond_to do | format |
      format.js {render :layout => false}
    end
  end
  
  private
  
  #permite definir a secção blog como selecionada na barra superior
  def is_blog_section
    @is_blog_section = 1;
  end

end
