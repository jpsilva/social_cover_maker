class ImagesController < ApplicationController
  before_filter :create_images_array, :only => [:create]
  respond_to :json

  def index
    @images_ids = []
    @images = []
    if session[:images]
      @size = session[:images].size.to_s
      @images_ids = session[:images]
    end

    @images_ids.each do |image_id|
      @images << Image.find(image_id)
    end
  end

  def show
    @image = Image.find(params[:id])
  end

  def new
    @image = Image.new
  end

  def edit
  end

  def create
            print "\n\nONE"
    @image = Image.new(params[:image])
            print "\n\nTWO"
    @item_id = @image.item_id
    @item = Item.find(@item_id)
    @template_id = params[:template_id]
    @locale = params[:locale]

    respond_to do |format|
      if @image.save
        @image.save##para forçar o update e o carrierwave fazer resize da imagem original para o que o item pede
        session[:images] << @image.id
      end
      format.js
    end
  end

  def update
    @image = Image.find(params[:id])
    if @image.update_attributes(params[:image])
      @item_id = @image.item_id
      render 'crop_success.js.erb'
    end
 end

 def crop
   if Image.exists?(params[:id])
     @image = Image.find(params[:id])
     @item = Item.find(params[:item_id] )
     template = @item.template
     @website = template.website.name
     @locale = params[:locale]

    if template.crop_type == 0
      image_file = @image.get_resized
      @width = image_file['width']
      @height = image_file['height']
      render :layout => false
    else
      image_file = @image.get_original
      @width = image_file['width']
      @height = image_file['height']
      render :layout => false, :partial => 'jwindowcrop'
    end
  else
     render :js => t("editing_image_expired"), :status => 403
  end
 end

  private
    def create_images_array
      if session[:images].nil?
        session[:images] = []
      end
    end
end
