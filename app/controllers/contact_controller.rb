class ContactController < ApplicationController
  before_filter :is_about_section, :only =>[:new]
  before_filter :is_contact_section, :only => [:new]
  def new
    @title = t('contact')
    @message = ContactMessage.new
  end

  def create
    @message = ContactMessage.new(params[:contact_message])

    if @message.valid?
      ContactMailer.new_message(@message).deliver
      redirect_to contact_success_path
    else
      print @message.errors.messages
      flash.now.alert = "Please fill all fields."
      is_contact_section
      is_about_section
      render :new
    end
  end
  
  def contact_success
    is_contact_section
    is_about_section
    @title = t('contact')
  end

  private
  def is_about_section
    @is_about_section = 1;
  end

  def is_contact_section
    @is_contact_section = 1;
  end
end
