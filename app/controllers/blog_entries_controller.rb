class BlogEntriesController < ApplicationController
  before_filter :prepare_menu_data, :only => [:index, :show, :index_by_tag, :index_by_category]
  before_filter :is_blog_section
  before_filter :blog_entries_per_page, :only => [:index, :index_by_tag, :index_by_category]
  
  # GET /blog_entries
  # GET /blog_entries.json
  def index
    @blog_entries = BlogEntry.paginate(:per_page => blog_entries_per_page, :page => params[:page])
    
    @title = t('blog')


    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @blog_entries }
    end
  end

  # GET /blog_entries/1
  # GET /blog_entries/1.json
  def show
    @blog_entry = BlogEntry.find_by_slug(params[:slug])
    
     
    @len = @blog_entry.tags.count
    @count = 0
    @comments_count = BlogComment.where(:blog_entry_id => @blog_entry.id, :approved => true).count
    @quantity = 2
    all_blog_entry_comments = @blog_entry.blog_comments
    @all_blog_entry_comments_size = all_blog_entry_comments.where(:approved=>true).size
    @blog_comments = all_blog_entry_comments.where(:approved=>true).limit(@quantity)
    @offset = 0
    @new_comment = BlogComment.new
    
    #for title and meta datas
    @title = @blog_entry.title
    @meta_keywords = BlogEntriesTag.get_tags_keywords(@blog_entry)
    @meta_description = @blog_entry.excerpt.truncate(200)
    
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @blog_entry }
    end
  end

  # GET /blog_entries/new
  # GET /blog_entries/new.json
  def new
    @blog_entry = BlogEntry.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @blog_entry }
    end
  end

  # GET /blog_entries/1/edit
  def edit
    @blog_entry = BlogEntry.find(params[:id])
  end

  # POST /blog_entries
  # POST /blog_entries.json
  def create
    @blog_entry = BlogEntry.new(params[:blog_entry])

    respond_to do |format|
      if @blog_entry.save
        format.html { redirect_to @blog_entry, notice: 'Blog entry was successfully created.' }
        format.json { render json: @blog_entry, status: :created, location: @blog_entry }
      else
        format.html { render action: "new" }
        format.json { render json: @blog_entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /blog_entries/1
  # PUT /blog_entries/1.json
  def update
    @blog_entry = BlogEntry.find(params[:id])

    respond_to do |format|
      if @blog_entry.update_attributes(params[:blog_entry])
        format.html { redirect_to @blog_entry, notice: 'Blog entry was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @blog_entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /blog_entries/1
  # DELETE /blog_entries/1.json
  def destroy
    @blog_entry = BlogEntry.find(params[:id])
    @blog_entry.destroy

    respond_to do |format|
      format.html { redirect_to blog_entries_url }
      format.json { head :no_content }
    end
  end

  def index_by_tag
    @blog_entries = []
    tag = Tag.where(:slug => params[:slug]).first
     @title = tag.name
    if tag
      BlogEntry.all.each do |be|
        if be.tags.include? tag
          @blog_entries << be
        end
      end
      @blog_entries = tag.blog_entries.paginate(:per_page => blog_entries_per_page, :page => params[:page])
    end

    render :index
  end

  def index_by_category
    @blog_entries = []
    category = BlogCategory.where(:slug => params[:slug]).first
     @title = category.name
    if category
      BlogEntry.all.each do |be|
        if be.blog_category.id == category.id
          @blog_entries << be
        end
      end
      @blog_entries = BlogEntry.where(:blog_category_id => category.id).paginate(:per_page => blog_entries_per_page, :page => params[:page])

    end

    render :index
  end

  private
    def prepare_menu_data
      @categories = BlogCategory.all
      @posts_by_year = BlogEntry.posts_by_year
    end
    
    #permite definir a secção blog como selecionada na barra superior
    def is_blog_section
      @is_blog_section = 1;
    end

  def blog_entries_per_page
    @blog_entries_per_page = 2
  end
end
