ActiveAdmin.register BlogEntry do
	form do |f|                         
    f.inputs "BlogEntry Details" do       
      f.input :blog_category
      f.input :title                  
      f.input :html
      f.input :excerpt
      f.input :slug  
    end                               
    f.buttons                         
  end     
end
