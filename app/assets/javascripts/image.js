$(document).ready(
  function(){
    //style all the file fields
    //SI.Files.stylizeAll();
    $.facebox.settings.closeImage = '/assets/closelabel.png'
    $.facebox.settings.loadingImage = '/assets/loading.gif'
    
    // Starts actions when any of the (image) file fields are changed
    $(document).on('change', '.file',
      function(){
        var id = $(this).attr('id');
        if ($(this).val() != "") {
          
          var error_message = $('.item_container').attr('too_big_error');
          var valid = validImageSize($(this).attr('id'), error_message);
          
          if (valid){
            $("#uploading_" + id).show();
            upload(id);
          }
          
        };
    });

    //setTextFieldsTrigger();
    setCropSubmissionTrigger();
    setCloseCropWindowTrigger();
    setTextFieldsBeforeSubmit();
  }
);

// Submits the form that includes the file field that was changed
function upload (file_field_id){
  var form_id = "#form_for_" + file_field_id;
  
  //se já tiver enviado alguma imagem, avisar que deixou de estar preparada
  var item_id = file_field_id.replace(/\D/g, '');
  if ($("#hidden_" + item_id).val() != ""){
    imageNotReady();
  }
  
  $(form_id).submit();
}

function activateSubmitButton(){
  var aux = true;
  $('.file').each(function(){
    if( $(this).val() == "" ){
      aux = false;
    }
  })
  if(aux){
    $('#submit_images').attr('disabled', false);
    $('#uniform-submit_images').removeClass('disabled');
  }
}

// Starts actions when any of the text fields are changed
/*function setTextFieldsTrigger(){
  $(document).on('change', '.text_field',
      function(){
        var item_id = $(this).attr('name');
        var valor = $(this).val();
      	$('#hidden_' + item_id).val(valor);
      }
  );
}*/

//opens popup to crop the image
function openCropWindow(image_id, item_id, locale){
  params = {};
  params['item_id'] = item_id;
  params['locale'] = locale;
  
  $.ajax({
      url: '/images/' + image_id + '/crop' ,
      data: params,
      success: function (html) {
        jQuery.facebox(function() {
          
          $.facebox(html);
          $('.crop_window_buttons').uniform();
          //$('.qtip.ui-tooltip').qtip('hide');
          
        });
        $(".close_image").hide();        
      },
      error: function(jqXHR, textStatus, errorThrown) {
        alert(jqXHR.responseText);//display errors in alert
      }
  });
}

//when crop form submits -> close facebox and show loading icon and remove current preview
function setCropSubmissionTrigger(){
  $(document).on('submit', '#crop_form',
    function(event, data, status, xhr) {
      //get item id
      var id = $(this).find(".item_value").val();

      //remove current preview
      $( '#preview_for_item_' + id ).remove();
      
      //close facebox
      jQuery(document).trigger('close.facebox');
      
      //show loading icon
      $("#cropping_item_" + id).show();
      
      //reduce number of ready images
      imageNotReady();
    });
}

function setCloseCropWindowTrigger(){
  $(document).on('click', '#closeCropWindowButton',
    function(e){
      e.preventDefault();
      jQuery(document).trigger('close.facebox');
  });
}

function imageReady(){
  var previous_ready_count =  parseInt($('#submit_images').attr('ready'));
  var new_ready_count = previous_ready_count + 1 ;
  
  $('#submit_images').attr('ready', new_ready_count);

  var item_size_string = $('#submit_images').attr('item_size');
  var item_size = parseInt(item_size_string);
  
  if(new_ready_count == item_size ){
    $('#submit_images').attr('disabled', false);
    $('#uniform-submit_images').removeClass('disabled');
  }
}

function imageNotReady(){
  var previous_ready_count =  parseInt($('#submit_images').attr('ready'));
  var new_ready_count;

  if ((previous_ready_count - 1) < 0){
    new_ready_count = 0;
  }else{
    new_ready_count = previous_ready_count - 1;
  }

  $('#submit_images').attr('ready', new_ready_count);
  
  $('#submit_images').attr('disabled', true);
  $('#uniform-submit_images').addClass('disabled');
}

function validImageSize(file_id, error_message){
  
  var input, file;

  // (Can't use `typeof FileReader === "function"` because apparently
  // it comes back as "object" on some browsers. So just see if it's there
  // at all.)

  /*if (!window.FileReader) {
      alert("The file API isn't supported on this browser yet.");
      return true;
  }
  */
  input = document.getElementById(file_id);
  /*
  if (!input) {
      alert("Um, couldn't find the fileinput element.");
  }
  else if (!input.files) {
      alert("This browser doesn't seem to support the `files` property of file inputs.");
  }
  else if (!input.files[0]) {
      alert("Please select a file before clicking 'Load'");
  }
  else {*/
      file = input.files[0];
      
      if (file.size >  5242880 ){
        //alert("File " + file.name + " is " + file.size + " bytes in size");
        alert(error_message);
        return false;
      }      
  //}
  
  return true;
}

function setTextFieldsBeforeSubmit(){
  $(document).on('click', '#submit_all_images',
    function(e){
      $('.text_field').each(function(index) {
        var item_id = $(this).attr('name');
        var valor = $(this).val();
        $('#hidden_' + item_id).val(valor);
      })
  }); 
}