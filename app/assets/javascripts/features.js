function showFeature(){
$('#button1').click(function() {
if ($('#non-2').is(":visible")) 
  $('#non-2').hide();
else {
  $('#non-2').show();
}

});
}

$(document).ready(
  function(){
    launchVideo();
  }
);

function launchVideo(){
  $(document).on('click', ".youtube",
    function(){
      var locale = $(this).attr('locale');
      var link = $(this).attr('link');
      var params = {};
      params["link"] = link;
      
      $.ajax({
          url: '/' + locale + '/youtube_lightbox',
          data: params,
          success: function (html) {
            jQuery.facebox(function() {
              $.facebox(html);
            });
          },
          error: function(jqXHR, textStatus, errorThrown) {
            alert(jqXHR.responseText);//display errors in alert
          }
      });

      
  });  
  
  
  $(document).bind('afterClose.facebox', function() {
    $(".content").empty();    
  })
}

