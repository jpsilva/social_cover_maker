$(document).ready(function()
{    
  prepareTemplateTips();
});

function prepareTemplateTips(){
  //tip para a ajuda de cada item
  /*$('.item_info_buttons').qtip(
	{
		content: {
		  title: {
      					text: function(api) {
                				return $(this).attr('qtip_title');
                			},
      					button: true
      				},
			text: function(api) {
      			  return '<img src="' + $(this).attr('qtip_url') + '" alt="Help_Image">';
      		  }
		},
		show: {
    				event: 'click',
    				solo: true // Only show one tooltip at a time
    			},
    hide: 'unfocus',
    style: {
  		classes: 'ui-tooltip-shadow ui-tooltip-rounded ui-tooltip-youtube item_info_tip',
      width: 350,
      padding: 0
  	},
  	position: {
		  my: 'top left',  // Position my top left...
		  at: 'bottom left', // at the bottom right of...
		  //target: $('.item_info_buttons') // my target
	  }
	});	

  //------------------------------------------------------------------------------------------
	//tip para o upload de ficheiros
	$('.file').qtip({
    content: {
      text: function(api) {
      				return $(this).attr('info');
      			},
    },
    position: {
		  my: 'top left',  // Position my top left...
		  at: 'bottom left', // at the bottom right of...
//		  target: $(this) // my target
	  },
    style: {
  		classes: 'ui-tooltip-shadow ui-tooltip-rounded ui-tooltip-youtube tooltip_padding'
  	}    	
	});
	*/

  //------------------------------------------------------------------------------------------	
	//tip dowload da imagem de profile gplus
  $('.profile_gplus').qtip({
    content: {
      text: function(api) {
      				return $(this).attr('download_profile_help_text') +"<p>"+ $(this).attr('functionalities_path') +"</p>";
      			},
    },
    style: {
  		classes: 'ui-tooltip-shadow ui-tooltip-rounded ui-tooltip-youtube tooltip_padding',
  		width:350
  	},
  	overwrite: false,
  	position: {
  		my: 'top left',
  		at: 'bottom left',
  	},
  	show: {
    			event: 'mouseenter',
    			solo: true, // Only show one tooltip at a time
    			effect: function(offset) {
          			$(this).fadeIn(450); // "this" refers to the tooltip
          },
          delay: 300
    			},
		hide: {
  		event: 'mouseleave',
      delay: 200,
      fixed: true
    }
  });

  //------------------------------------------------------------------------------------------
	//tip dowload da imagem de cover gplus
  $('.cover_gplus').qtip({
    content: {
      text:  function(api) {
      				return $(this).attr('download_cover_help_text') +"<p>"+ $(this).attr('functionalities_path') +"</p>";
      			},
    },
    style: {
  		classes: 'ui-tooltip-shadow ui-tooltip-rounded ui-tooltip-youtube tooltip_padding',
  		width:350
  	},
  	overwrite: false,
  	position: {
  		my: 'top center',
  		at: 'bottom center',
  		adjust: {
      			x: -40
      		}
  	},
  	show: {
    			event: 'mouseenter',
    			solo: true, // Only show one tooltip at a time
    			effect: function(offset) {
          			$(this).fadeIn(450); // "this" refers to the tooltip
          },
          delay: 300
    			},
		hide: {
  		event: 'mouseleave',
      delay: 200,
      fixed: true
    }
});

  //------------------------------------------------------------------------------------------  
  //tip para a ajuda de cada item do gplus
  $('.item_container_gplus').qtip(
	{
		content: {
/*		  title: {
      					text: function(api) {
                				return "INFO:";
                			},
      					button: true
      				},*/
			text: function(api) {
			        var count = $(this).attr('count');
              var template_key = $(this).attr('template_key');
              var image_path = '/templates/' + template_key + '/h_' + count + '.jpg';
              
              var content = $(this).attr('info') + $(this).attr('qtip_title');
              content += '<img src="' + image_path + '" alt="Help_Image">';
              
      			  return content;
      		  }
		},
		show: {
    				event: 'mouseenter',
    				solo: true, // Only show one tooltip at a time
    				effect: function(offset) {
            			$(this).fadeIn(450); // "this" refers to the tooltip
            },
            delay: 300
    			},
    hide: {
      		event: 'mouseleave',
          delay: 200,
          fixed: true
    },
    style: {
  		//classes: 'ui-tooltip-shadow ui-tooltip-rounded ui-tooltip-youtube item_info_tip',
  		classes: 'ui-tooltip-shadow ui-tooltip-rounded item_info_tip_gplus',
      width: 350,
      padding: 0
  	},
  	position: {
		    //target: this, // my target
/*      adjust: {
      			y: -100,
      			x: -200
      		},*/
      my: 'bottom center',  // Position my top left...
		  at: 'top center', // at the bottom right of...		
	  }
	});

  //------------------------------------------------------------------------------------------  
  //tip para a ajuda de cada item do facebook
  $('.item_container_facebook').qtip(
	{
		content: {
/*		  title: {
      					text: function(api) {
                				return "INFO:";
                			},
      					button: true
      				},*/
			text: function(api) {
			        var count = $(this).attr('count');
              var template_key = $(this).attr('template_key');
              var image_path = '/templates/' + template_key + '/h_' + count + '.jpg';

              var content = $(this).attr('info') + $(this).attr('qtip_title');
              content += '<img src="' + image_path + '" alt="Help_Image">';

      			  return content;
      		  }
		},
		show: {
    				event: 'mouseenter',
    				solo: true, // Only show one tooltip at a time
    				effect: function(offset) {
            			$(this).fadeIn(450); // "this" refers to the tooltip
            },
            delay: 300
    			},
    hide: {
      		event: 'mouseleave',
          delay: 200,
          fixed: true
    },
    style: {
  		classes: 'ui-tooltip-shadow ui-tooltip-rounded item_info_tip_facebook',
      width: 350,
      padding: 0
  	},
  	position: {
      my: 'bottom center',  // Position my top left...
		  at: 'top center', // at the bottom right of...		
	  }
	});
	
  //------------------------------------------------------------------------------------------	
  //tip dowload da imagem de profile fb
  $('.profile_fb').qtip({
    content: {
      text: function(api) {
      				return $(this).attr('download_profile_help_text') +"<p>"+ $(this).attr('functionalities_path') +"</p>";
      			},
    },
    style: {
  		classes: 'ui-tooltip-shadow ui-tooltip-rounded ui-tooltip-youtube tooltip_padding',
  		width:350
  	},
  	overwrite: false,
  	position: {
  		my: 'top left',
  		at: 'center right'
  	},
  	show: {
    			event: 'mouseenter',
    			solo: true, // Only show one tooltip at a time
    			effect: function(offset) {
          			$(this).fadeIn(450); // "this" refers to the tooltip
          },
          delay: 300
    			},
		hide: {
  		event: 'mouseleave',
      delay: 200,
      fixed: true
    }
  });

  //------------------------------------------------------------------------------------------
  //tip dowload da imagem de cover fb
  $('.cover_fb').qtip({
    content: {
      text:  function(api) {
      				return $(this).attr('download_cover_help_text') +"<p>"+ $(this).attr('functionalities_path') +"</p>";
      			},
    },
    style: {
  		classes: 'ui-tooltip-shadow ui-tooltip-rounded ui-tooltip-youtube tooltip_padding',
  		width:350
  	},
  	overwrite: false,
  	position: {
  		my: 'top right',
  		at: 'bottom right',
  		adjust: {
      			x: -40
      		}
  	},
  	show: {
    			event: 'mouseenter',
    			solo: true, // Only show one tooltip at a time
    			effect: function(offset) {
          			$(this).fadeIn(450); // "this" refers to the tooltip
          },
          delay: 300
    			},
		hide: {
  		event: 'mouseleave',
      delay: 200,
      fixed: true
    }
});

  //------------------------------------------------------------------------------------------



};


