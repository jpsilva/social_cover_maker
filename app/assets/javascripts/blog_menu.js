$(document).ready(
  function(){
    hideAndShowMenuOptions();
  }
);


// Hide/show submenu's on menu click
function hideAndShowMenuOptions(){
  $('.menuItemText').click(
      function(){
        var menu_id = $(this).attr('id');
        var submenu_id = "#" + menu_id + "_submenu";

        $(submenu_id).slideToggle();
      }
  );
}