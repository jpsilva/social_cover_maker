$(document).ready(function()
{    
  faqElementTrigger();
  goToFaqTop();
});

function faqElementTrigger(){
  $(document).on('click', ".faqLink",
    function(e){
      e.preventDefault();
      goToByScroll($(this).attr('id') + "_answer");
  });
}

function goToFaqTop(){
  $(document).on('click', ".link_to_faq_top",
    function(e){
      e.preventDefault();
      goToByScroll("faq_top");
  }); 
}