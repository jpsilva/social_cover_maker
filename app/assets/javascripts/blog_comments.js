$(document).ready(
  function(){
    setCommentsLinkTrigger();

  }
);

// sets trigger that scrolls to comments when comments link is clicked 
function setCommentsLinkTrigger(){
  $('#link_to_comments').click(function(event){
    goToByScroll('comment_form');
  });
}