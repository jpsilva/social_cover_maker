$(document).ready(
  function(){
    showLoadingOnDownloadPage();
    
    changeSelectedTab();
    
    Hyphenator.run();
    
    var timeOut, timeOut2, timeOut3;
    orderButtons(timeOut);
    timeOrderButtons(timeOut2);
//    showItemInfo(timeOut3);
//    submitAllImagesTrigger();
    hideAndShowSubmitTemplateExamples();
    
    showExamplesButtonTrigger();//botão que ao clicar, mostra o exemplo no show dos templates
    //showHelpImageTrigger();//ao passar o rato por cima de um item diferente, a imagem de ajuda é apresentada
  
  floatingDiv();

  closeFloatingDiv();

  openUserExamplesSubmission();

  setCloseSubmitExamplesTrigger();
  
  }
);


function setCloseSubmitExamplesTrigger(){
  $(document).on('click', '#SubmitUserExampleButton',
    function(e){
      jQuery(document).trigger('close.facebox');
  });
}

function openUserExamplesSubmission(){
  $('#UserExampleBox').live('click', function(){
    $.facebox.settings.opacity = 0.7
    $.facebox.settings.transparency = 0
    $.facebox($('#UserExampleDiv').html());

});

}

function closeFloatingDiv(){
   $('#SubmitUserExampleButton').live('click', function() {
      $('.SubmitUserExample').hide();
  });
}

function floatingDiv(){
  $('SubmitUserExample').addFloating(
  {
    targetRight: 10,
    targetTop: 10,
    snap: true
  });
}

function socialFiltersToggle(){
  $(function(){
    $(".img-swap").live('click', function() {
      
      if ($(this).attr("class") == "img-swap") {
        this.src = this.src.replace("_on","_off");
      } else {
        this.src = this.src.replace("_off","_on");
      }
      $(this).toggleClass("on");
    });
  });  
}


function showLoadingOnDownloadPage(){
  $("#waiting_for_download_links").show();
}

function requestDownloadLinks(locale, templateId, imageProcessingJobId){
  params = {};
  params['imgPJ_id'] = imageProcessingJobId;
  $.ajax({
      url: '/' + locale + '/themes/' + templateId + '/request_download_links',
      data: params,
      success: function (html) {        
      },
      error: function(jqXHR, textStatus, errorThrown) {
        alert(jqXHR.responseText);//display errors in alert
      }
  });
}

function setCommentsTabTrigger(templateId){
	params = {};
	$.ajax({
  		url: '/themes/'+templateId +'/tab_comments',
  		data: params,
  		success: function (html) {
  		  $('.commentbutton').uniform();    
  		},
  		error: function(jqXHR, textStatus, errorThrown) {
    		alert(jqXHR.responseText);//display errors in alert
  		}
  });
}

function setSubmitTabTrigger(templateId){  
  params = {};
	$.ajax({
  		url: '/themes/'+templateId +'/tab_items',
  		data: params,
  		success: function (html) { 
  			//$('input[type=file], input[type=submit]').uniform();
  			$('.file, .item_info_buttons, .crop_buttons, .submit_template_button').uniform();
  			      
  		},
  		error: function(jqXHR, textStatus, errorThrown) {
    		alert(jqXHR.responseText);//display errors in alert
  		}
  });    
}

function setOutputTabTrigger(locale, templateId){  
  params = {};
	$.ajax({
  		url: '/' + locale + '/themes/'+templateId +'/tab_output',
  		data: params,
  		success: function (html) {        
  		  $('#download_profile, #download_cover').uniform();
  		},
  		error: function(jqXHR, textStatus, errorThrown) {
    		alert(jqXHR.responseText);//display errors in alert
  		}
  });    
}

function setSimilarsTabTrigger(locale, templateId){
	params = {};
  params['locale'] = locale;
  
	$.ajax({
  		url: '/themes/'+templateId +'/tab_similars',
  		data: params,
  		success: function (html) {        
  		},
  		error: function(jqXHR, textStatus, errorThrown) {
    		alert(jqXHR.responseText);//display errors in alert
  		}
  });
}
  
function changeSelectedTab(){
  $(".tabrow li").click(function(e) {
    if ($(this).attr('class') != 'disabled' ){
      e.preventDefault();
      $("li").removeClass("selected");
      $(this).addClass("selected");
    }
  });
}

function orderButtons(timeOut){
  $(document).on('click', "#orderingOptionsButton",
    function(e){
      if($('#orderMenu').css('display') == 'none'){ 
         $('#orderMenu').show();
         $('#timeOrderMenu').hide();
      } else { 
        $('#orderMenu').hide();
      }
      
  });

  $('#orderMenu, #orderingOptionsButton').hover(
    function(e) {
      clearTimeout(timeOut);
    },
    function() {
      timeOut = setTimeout("$('#orderMenu').hide();", 500);
    }
  );
}

function timeOrderButtons(timeOut2){
  $(document).on('click', "#timeOrderingOptionsButton",
    function(e){
      if($('#timeOrderMenu').css('display') == 'none'){ 
         $('#timeOrderMenu').show();
         $('#orderMenu').hide();
      } else { 
        $('#timeOrderMenu').hide();
      }
      
  });

  $('#timeOrderMenu, #timeOrderingOptionsButton').hover(
    function(e) {
      clearTimeout(timeOut2);
    },
    function() {
      timeOut2 = setTimeout("$('#timeOrderMenu').hide();", 500);
    }
  );
}

function showItemInfo(timeOut3){
//  $(document).on('click', ".item_info_buttons",
  $(".item_info_buttons").click(
    function(){
      var id = '#' + $(this).attr('id') + '_div';
      var item_div = "#div_for_item_" +  $(this).attr('item_id');

      if($(id).css('display') == 'none'){ 
        var pos = $(item_div).position();
        var width = $(item_div).outerWidth();
        var _top = pos.top + 20;
        var _left;
        
        if (pos.left > 300 || width > 400)
            _left = 142;
        else
          _left = pos.left + width - 240;

        $(id).css({
            position: "absolute",
            top: _top,
            left: _left
        }).show();
        
        timeOut3 = setTimeout("$('.itemInfo').hide();", 3000);             
      
      } else { 
        $(id).hide();
      }
    })
    
  $('.itemInfo, .item_info_buttons').hover(
    function(e) {
      clearTimeout(timeOut3);
    },
    function() {
      timeOut3 = setTimeout("$('.itemInfo').hide();", 500);      
    }
  );  
}

/*function submitAllImagesTrigger(){
  $(document).on('ajax:error', '#submit_all_images', 
    function(e, data, status, xhr){
      alert("session_expired");
      window.location = data.responseText;//display errors in alert
  })
}*/

function hideAndShowSubmitTemplateExamples(){
  $(document).on('click', "#submit_user_example_check_box",
    function(){
      var thisCheck = $(this);
      if (thisCheck.is (':checked'))
      {
        $('#extra_submit_user_example_info').removeClass('hide');
      }else{
        $('#extra_submit_user_example_info').addClass('hide');
      }
  });
}


function showExamplesButtonTrigger(){
  $(document).on('click', "#show_template_example_button",
    function(){
      $("#templateExamples").slideToggle();
  });  
}

/*function showHelpImageTrigger(){
  $(document).on('hover', ".item_container",
    function(){
      var count = $(this).attr('count');
      var template_key = $(this).attr('template_key');
      
      var imag_path = '/templates/' + template_key + '/h_' + count + '.jpg';
      //$('#help_image_container > img').attr('src', );
      
  });
}*/
