$(document).ready(
  function(){
    navigateThroughTemplateExamples();
    
    animateZoomIcon();

    setTemplateExampleTrigger();
    
    listenRequestTemplateExample();
  }
);

//sets trigger that will trigger the openTemplateExampleWindow function
function setTemplateExampleTrigger(){
  $(document).on('click', '.buttonZoom',
    function(){
      var slug = $(this).attr('id');
      var locale = $(this).attr('value');
      openWindowWithTemplateExamples(slug, locale);
  });
}

//opens popup to view template examples
function openWindowWithTemplateExamples(slug, locale){
  params = {};
  params['slug'] = slug;
  params['locale'] = locale;
  params['offset'] = 0;
  $.ajax({
      url: '/template_examples',
      type: "GET",
      data: params,
      success: function (html) {
        jQuery.facebox(function() {
          $.facebox(html);
        });        
      },
      error: function(jqXHR, textStatus, errorThrown) {
        alert(jqXHR.responseText);//display errors in alert
      }
  });
}

//listen for the success of the get other template example
function listenRequestTemplateExample(){
  $(document).on('ajax:success', '#request_previous_template_example, #request_next_template_example',
    function(event, xhr, status, other) {
      $('#template_examples_content').replaceWith(xhr);
    }
  );  
}

function navigateThroughTemplateExamples(){
  $("body").keydown(function(e) {
    if(e.keyCode == 37) { // left
      $('#request_previous_template_example').click();
    }
    else if(e.keyCode == 39) { // right
      $('#request_next_template_example').click();
    }
  });
}

function animateZoomIcon(){
  $('.buttonZoom > IMG').hover(function(){
  	$(this).animate({width:'70px', height:'70px'},{queue:false,duration:250});
  }, function(){
  	$(this).animate({width:'36px', height:'36px'},{queue:false,duration:250});
  });
}

function showNextExample(templateId, offset, locale){
	params = {};
	params['offset'] = offset;
	params['locale'] = locale;
	$.ajax({
  		url: '/themes/'+templateId +'/examples',
  		data: params,
  		success: function (html) {        
  		},
  		error: function(jqXHR, textStatus, errorThrown) {
    		alert(jqXHR.responseText);//display errors in alert
  		}
  });
 }
