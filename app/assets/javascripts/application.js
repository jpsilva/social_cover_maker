// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require jquery.remotipart
//= require jcrop
//= require_tree ../../../vendor/assets/javascripts/.
//= require_directory .

var timeout = null;//timeout para o slideshow
var time_between_slides = 3000;
var website = "";

jQuery(document).ready(function() {
	jQuery('#top-nav-logo').hide();
	
	$("ul.example1").simplecarousel({
		width : 400,
		height : 30,
		visible : 1,
		auto : 3000,
		fade: 400
	});
	
	//jquery css for inputs buttons etc
  $(function(){
    $("select, input:checkbox, input:radio, input:file, input:submit").uniform();
  });
	
	//trigger para a alteração do locale
	$(document).on('change', "#select_locale",function(){    
	  var redirect_url = $(this).attr('value');
	  window.location.href = redirect_url;
  });
  
  timeout = setInterval( "slideSwitch()", 0 );
});


/*** 
    Simple jQuery Slideshow Script
    Released by Jon Raasch (jonraasch.com) under FreeBSD license: free to use or modify, not responsible for anything, etc.  Please link out to me if you like it :)
***/
function slideSwitch() {
  
  
  var $active = $('#slideshow IMG.active');
  if ( $active.length == 0 ) $active = $('#slideshow IMG:last');

  // use this to pull the images in the order they appear in the markup
  var $next =  $active.next().length ? $active.next()
      : $('#slideshow IMG:first');

  if(website != $next.attr('website')){
    clearInterval(timeout);

    // uncomment the 3 lines below to pull the images in random order
  
    // var $sibs  = $active.siblings();
    // var rndNum = Math.floor(Math.random() * $sibs.length );
    // var $next  = $( $sibs[ rndNum ] );

    $active.addClass('last-active');
  
    //esconder/mostrar pelicula adequada
    if($next.attr('website') == 'Google+'){
      $('#gplus_slideshow_pelicule').first().animate({opacity: 1.0}).removeClass('last-active').addClass('active');
      $('#fb_slideshow_pelicule').first().animate({opacity: 0.0}).removeClass('active').addClass('last-active');
      website = "Google+";
    }else if($next.attr('website') == 'Facebook'){
      $('#fb_slideshow_pelicule').first().animate({opacity: 1.0}).removeClass('last-active').addClass('active');
      $('#gplus_slideshow_pelicule').first().animate({opacity: 0.0}).removeClass('active').addClass('last-active');
      website = "Facebook";
    }
  
    //alterar perfil exemplo
    $next.css({opacity: 0.0})
        .addClass('active')
        .animate({opacity: 1.0}, 1000, function() {
            $active.removeClass('active last-active');
      });
      
      timeout = setInterval( "slideSwitch()", time_between_slides );
  }    
}

function slideSwitchPrevious() {  
  var $active = $('#slideshow IMG.active');
  
  if ( $active.length == 0 ) $active = $('#slideshow IMG:last');
  
  // use this to pull the images in the order they appear in the markup

  var $prev =  ($active.prev().length > 0 && $active.prev().attr("src")) ? $active.prev() : $('#slideshow IMG:last');

  // uncomment the 3 lines below to pull the images in random order
  
  // var $sibs  = $active.siblings();
  // var rndNum = Math.floor(Math.random() * $sibs.length );
  // var $next  = $( $sibs[ rndNum ] );
  if(website != $prev.attr('website')){
    console.log("ENTREI:");
    clearInterval(timeout);
    $active.addClass('last-active');
  
    //esconder/mostrar pelicula adequada
    if($prev.attr('website') == 'Google+'){
      $('#gplus_slideshow_pelicule').first().animate({opacity: 1.0}).removeClass('last-active').addClass('active');
      $('#fb_slideshow_pelicule').first().animate({opacity: 0.0}).removeClass('active').addClass('last-active');
      website = "Google+";
    }else if($prev.attr('website') == 'Facebook'){
      $('#fb_slideshow_pelicule').first().animate({opacity: 1.0}).removeClass('last-active').addClass('active');
      $('#gplus_slideshow_pelicule').first().animate({opacity: 0.0}).removeClass('active').addClass('last-active');
      website = "Facebook";
    }
  
    //alterar perfil exemplo
    $prev.css({opacity: 0.0})
        .addClass('active')
        .animate({opacity: 1.0}, 1000, function() {
            $active.removeClass('active last-active');
        });        

    timeout = setInterval( "slideSwitch()", time_between_slides );    
  }
}

//$(function() {
//    setInterval( "slideSwitch()", 2000 );
//});

function goToByScroll(id){
		$('html,body').animate({scrollTop: $("#"+id).offset().top - 60},'slow');
}

//home:  what we offer redirect
function goToFeatures(locale){
	$(location).attr('href','/' + locale+'/features');
	//window.location("/features");
}

function goToRandomTemplate(locale, slug){
	$(location).attr('href','/' + locale +'/themes/' + slug)
}
