class Ad < ActiveRecord::Base
  attr_accessible :script
  has_many :templates
  validates :script, :presence => true
end
