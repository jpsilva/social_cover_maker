class BlogComment < ActiveRecord::Base
  before_create :user_is_well_known

  belongs_to :blog_entry
  attr_accessible :comment, :email, :name, :website, :approved, :blog_entry_id #ATT _blog_entry_id
  validates :comment, :presence => true
  validates :email,
            :presence => true,
            :format => { :with => /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i }
  validates :name, :presence => true

  #only auto approve comments for users with at least 5 Blog Comments
  def user_is_well_known
    user_posts = BlogComment.where(:email=>self.email, :name=>self.name, :approved => true)
    if user_posts.size >= 5
      self.approved = true
    end
  end

end
