class TagsTemplate < ActiveRecord::Base
  attr_accessible :tag_id, :template_id
  belongs_to :tag
  belongs_to :template

  #permite buscar as tags de um template separadas por ',' para adicionar ás meta keywords
  def self.get_tags_keywords(template)
    tags = template.tags

    len = tags.count
    count = 1;

    tags.each do |tag|
      if count == 1
        @keywords = ''
      end

      @keywords = @keywords + tag.name
      if count != len
        @keywords = @keywords + ','
      end
      count = count + 1;
    end
    @keywords
  end

end
