class ContactMessage

  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming

  attr_accessor :name, :email, :subject, :body, :answer

  validates :name, :email, :subject, :body, :presence => true
  validates :email, :format => { :with => /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i }, :presence => true
  validates :subject, :presence => true
  validates :body, :presence => true
  validates :answer, :presence => true
  validate :correct_answer

  def initialize(attributes = {})
    attributes.each do |name, value|
      send("#{name}=", value)
    end
  end

  def persisted?
    false
  end

  def correct_answer
    unless (answer == "blue" || answer == "azul")
      errors.add(:answer, :incorrect_answer)
    end
  end


end