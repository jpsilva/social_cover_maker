class BlogEntriesTag < ActiveRecord::Base
  belongs_to :blog_entry
  belongs_to :tag
   attr_accessible :blog_entry_id, :tag_id
  
   #permite buscar as tags de um post separadas por ',' para adicionar ás meta keywords
  def self.get_tags_keywords(blog_entry)
    tags = blog_entry.tags

    len = tags.count
    count = 1;

    tags.each do |tag|
      if count == 1
        @keywords = ''
      end

      @keywords = @keywords + tag.name
      if count != len
        @keywords = @keywords + ','
      end
      count = count + 1;
    end
    @keywords
  end
end
