class BlogCategory < ActiveRecord::Base
  attr_accessible :description, :name
  has_many :blog_entries

  after_create :make_slug

  def make_slug
     @slug = Utils.to_slug(self.name.dup, self)
     self.slug = @slug
     self.save
  end
end
