class Item < ActiveRecord::Base
  #ATT Campos :item_type_id e :template_id adicionados para ser possível gerar as seeds
  attr_accessible :description, :item_type_id, :template_id, :size, :width, :height

  belongs_to :template
  belongs_to :item_type
end
