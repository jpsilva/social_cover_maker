class BlogEntry < ActiveRecord::Base
  attr_accessible :html, :slug, :title, :excerpt, :blog_category_id #att adicionado a' mao
  validates :slug, :uniqueness => true, :presence => true
  belongs_to :blog_category
  has_many :tags, :through => :blog_entries_tags
  has_many :blog_entries_tags
  has_many :blog_comments

  after_create :make_slug

  def self.months
    ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december']
  end

  def self.posts_by_year
    posts_by_year = {}
    today = Date.today
    current_month = today.month
    current_year = today.year

    (2012..current_year).to_a.each do |year|
      posts_by_month = {}

      year == current_year ? max_month = (current_month-1) : max_month = 11##para acabar no mês actual
      (0..max_month).to_a.each do |i|
        start = DateTime.new(year,i+1,1)
        if (i+2) != 13
          finish = DateTime.new(year,i+2,1)
        else
          finish = DateTime.new(year+1,1,1)
        end
        posts_by_month[months[i]] = BlogEntry.where(:created_at => (start..finish))
      end
      posts_by_year[year.to_s] = posts_by_month
    end
    posts_by_year
  end

  def make_slug
     @slug = Utils.to_slug(self.title.dup, self)
     self.slug = @slug
     self.save
  end

end
