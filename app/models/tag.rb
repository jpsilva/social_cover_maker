class Tag < ActiveRecord::Base
   attr_accessible :name, :slug
   has_many :templates, :through => :tags_templates
   has_many :tags_templates
   has_many :blog_entries, :through => :blog_entries_tags
   has_many :blog_entries_tags

   after_create :make_slug

   def make_slug
      @slug = Utils.to_slug(self.name.dup, self)
      self.slug = @slug
      self.save
   end
end
