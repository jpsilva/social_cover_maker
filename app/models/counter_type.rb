class CounterType < ActiveRecord::Base
  attr_accessible :name
  has_many :counters
end
