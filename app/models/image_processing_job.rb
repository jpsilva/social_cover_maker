class ImageProcessingJob < ActiveRecord::Base
  attr_accessible :finished, :success, :session_id
  belongs_to :template
#  validates_uniqueness_of :session_id, :scope => :template_id
  #validates :session_id, :uniqueness=> { :scope => :template_id }, :presence => true
  validates :session_id, :presence => true
  validates :template_id, :presence => true
  before_destroy :delete_enclosing_folder

  def delete_enclosing_folder
    FileUtils.rm_rf("public/downloads/#{id}/")
  end

  def self.delete_old_images
    self.where("created_at < :minutes", {:minutes => 15.minutes.ago.utc}).each do |img|
      img.destroy
    end
  end
end
