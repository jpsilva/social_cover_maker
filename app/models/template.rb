class Template < ActiveRecord::Base
  attr_accessible :description, :icon, :name, :website_id, :output, :template_category_id, :crop_type, :key, :public #ATT adicionados campos :template_category_id, :website_id, :output, :key, :public devido às seeds. deve-se eliminar
  after_create :make_slug, :initialize_counters

  default_scope where(:public => true)

  has_many :tags, :through => :tags_templates
  has_many :tags_templates
  belongs_to :template_category
  has_many :items
  has_many :counters
  belongs_to :website
  has_many :template_comments
  has_many :template_examples
  has_many :image_processing_jobs
  belongs_to :ad



  ##label = 0   =>  normal (prioridade baixa)
  ##label = 1   =>  beta (prioridade média)
  ##label = 2   =>  alpha (prioridade alta)

  #crop_type = 0 => jcrop
  #crop_type = 1 => jwindowcrop

  #execution of the template script
  def self.exec_script(template_id, script_params, output_path)
    case template_id
    when 1
      Template1.template1(script_params)
    when 7
      Template7.template7(script_params, output_path)
    when 8
      Template8.template8(script_params, output_path)
    when 9
      Template9.template9(script_params, output_path)
    when 10
      Template10.template10(script_params, output_path)
    when 13
      Template13.template13(script_params, output_path)
    when 14
      Template14.template14(script_params, output_path)
    else
      Template9.template9(script_params, output_path)
    end
  end

  def make_slug
     @slug = Utils.to_slug(self.name.dup, self)
     self.slug = @slug
     self.save
  end

  def self.months
    ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december']
  end

  def self.templates_by_year
    templates_by_year = {}
    today = Date.today
    current_month = today.month
    current_year = today.year

    (2012..current_year).to_a.each do |year|
      templates_by_month = {}

      year == current_year ? max_month = (current_month-1) : max_month = 11##para acabar no mês actual
      (0..max_month).to_a.each do |i|
        start = DateTime.new(year,i+1,1)
        if (i+2) != 13
          finish = DateTime.new(year,i+2,1)
        else
          finish = DateTime.new(year+1,1,1)
        end
        templates_by_month[months[i]] = Template.where(:created_at => (start..finish))
      end
      templates_by_year[year.to_s] = templates_by_month
    end
    templates_by_year
  end

  #get templates by week, month, all for Views or executions 
  def self.get_templates_by_views_or_executions(templates, time, type)
    today = Date.today
    week_number = today.cweek - today.at_beginning_of_month.cweek

    if week_number > 4 or week_number < 1
      week_number = 1;
    end

    type = %w[Views Executions].include?(type) ? type : "Views"

    order_field = Counter.column_names.include?(time) ? time : 'week'<< week_number.to_s


      templates.joins("LEFT JOIN counters on counters.template_id = templates.id 
                      LEFT JOIN counter_types on counters.counter_type_id = counter_types.id").where("
                      counter_types.name = '#{type}' or counter_types.name is null").
                      order('counters.' << order_field.to_s << ' desc')
  end

  def self.get_templates_by_date
    Template.order('created_at desc')
  end


  def self.get_order_types
    ["date", "views", "executions"]
  end

  def self.get_order_periods_of_time
    ["week", "month", "all"]
  end

  def self.get_similars(template_id)
    @template = Template.find_by_id(template_id)
    @tags = @template.tags
    
    @templates = nil
    #@tags.each do |tag|
      @templates = Template.joins("INNER JOIN tags_templates on templates.id = template_id").where('tag_id in (?) and templates.id != ?', @tags, template_id).uniq
    #end

    @templates
  end

  def initialize_counters
    Counter.initialize_records(self)
  end

  def self.get_top_ten_templates
    slide_templates = Template.joins("LEFT JOIN counters on counters.template_id = templates.id").where("counter_type_id = 2").order('counters.month desc').limit(10)

    examples = TemplateExample.where('template_id in (?)', @slide_templates)
  end

  def self.get_random_template
    @template = Template.all.shuffle[0..1]
    @template.first
  end

  def self.get_top_six_templates
    slide_templates_gplus = Template.joins("LEFT JOIN counters on counters.template_id = templates.id").where("counter_type_id = 2 AND website_id = 1").order('counters.month desc').limit(3)
    slide_templates_fb = Template.joins("LEFT JOIN counters on counters.template_id = templates.id").where("counter_type_id = 2 AND website_id = 2").order('counters.month desc').limit(3)

#    examples_gplus = TemplateExample.where('template_id in (?)', slide_templates_gplus)
#    examples_fb = TemplateExample.where('template_id in (?)', slide_templates_fb)
    examples_gplus = []
    slide_templates_gplus.each do |g|
      examples_gplus.push(g.template_examples.first)
    end

    examples_fb = []
    slide_templates_fb.each do |f|
      examples_fb.push(f.template_examples.first)
    end

    examples = examples_gplus.zip(examples_fb).flatten.compact
  end
end
