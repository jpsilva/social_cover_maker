class Image < ActiveRecord::Base
  attr_accessible :file, :position, :crop_x, :crop_y, :crop_w, :crop_h, :item_id
  attr_accessor :crop_x, :crop_y, :crop_w, :crop_h, :item_id
  mount_uploader :file, ImageUploader
  after_update :crop_image
  before_destroy :delete_enclosing_folder
  validate :minimum_image_size

  validates :file,
      :presence => true,
      :file_size => { 
        :maximum => 5.megabytes.to_i
      }


  #o ImageUploader vai voltar a criar as imagens.
  def crop_image
    if crop_x.present?
      item = Item.find(item_id)
      if item.template.crop_type == 0
        recalculateCoords
      end
      file.recreate_versions!
    else
      file.recreate_versions!
    end
  end

  def get_resized
    path = "#{Rails.root}/public" + file_url(:resized)
    @image = MiniMagick::Image.open(path)
  end

  def get_original
    @image = MiniMagick::Image.open(file.path)
  end

  def recalculateCoords
    original = get_original
    resized = get_resized

    if (original['width'] != resized['width']) || (original['height'].to_f != resized['height'].to_f)
      width_division = original['width'].to_f / resized['width'].to_f
      height_division = original['height'].to_f / resized['height'].to_f

      self.crop_x = (self.crop_x.to_f * width_division).to_s
      self.crop_y = (self.crop_y.to_f * height_division).to_s
      self.crop_w = (self.crop_w.to_f * width_division).to_s
      self.crop_h = (self.crop_h.to_f * height_division).to_s
    end
  end

  def delete_enclosing_folder
    FileUtils.rm_rf("public/uploads/image/file/#{self.id}")
  end

  def self.delete_old_images
    self.where("created_at < :minutes", {:minutes => 4.minutes.ago.utc}).each do |img|
      img.destroy
    end
  end

  #validation for the minimum size of the uploaded image
  def minimum_image_size
    @image = get_original
    item = Item.find(item_id)
    if (@image['width'] < item.width) || (@image['height'] < item.height)
      errors.add(:file, "image_to_small")
    end
  end

end

