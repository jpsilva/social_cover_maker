class Counter < ActiveRecord::Base
  attr_accessible :all, :template_id, :week1, :week2, :week3, :week4, :month, :counter_type_id
  belongs_to :template
  belongs_to :counter_type
  
  def self.update_counter(template_id, counter_type_name)
    today = Date.today
    week_number = today.cweek - today.at_beginning_of_month.cweek
    week_days = {}
    week_month = {}
    
    @counter_type = CounterType.find_by_name(counter_type_name)
    
    @counter = Counter.where(:template_id => template_id, :counter_type_id => @counter_type.id).first#'template_id = ? and counter_type_id = ?',template_id, @counter_type.id )
    if @counter.nil?
      @counter = Counter.new
      @counter.week1 = 0;
      @counter.week2 = 0;
      @counter.week3 = 0;
      @counter.week4 = 0;
      @counter.month = 0;
      @counter.all = 0;
      @counter.template_id = template_id
      @counter.counter_type_id = @counter_type.id
      @counter.save
    end
    
    case week_number
    when 1
      if @counter.week4 > 0
        @counter.week1 = 0;
      end
      
      if @counter.week1 = 0;
        @counter.week2 = 0;
        @counter.week3 = 0;
        @counter.week4 = 0;
      end
      @counter.week1 += 1;
    when 2
      @counter.week2 += 1;
    when 3
      @counter.week3 += 1;
    when 4
      @counter.week4 += 1;
    else
      @counter.week1 += 1;
    end
    
    @counter.month = @counter.week1 + @counter.week2 + @counter.week3 + @counter.week4
    
    
    @counter.all += 1
    @counter.save
 end
 
 def self.initialize_records(template)
   row_views = {:week1 => 0, :week2 => 0, :week3 => 0, :week4 => 0, :month => 0, :all => 0, :counter_type_id => 2 }
   row_executions = {:week1 => 0, :week2 => 0, :week3 => 0, :week4 => 0, :month => 0, :all => 0, :counter_type_id => 1 }
   counter1 = template.counters.new(row_views)
   counter1.save!
   counter2 = template.counters.new(row_executions)
   counter2.save!
 end
end
