class ContactMailer < ActionMailer::Base
  default :from => "contact@socialcovermaker.com"
  default :to => "contact@socialcovermaker.com"

  def new_message(message)
    @message = message
    mail(:subject => "[SocialCoverMaker.com][Contact] #{message.subject}")
  end
end
