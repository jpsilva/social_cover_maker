module ApplicationHelper
  
  #dinamic title
  def title
   base_title = "Social Cover Maker"
   if @title.nil?
     base_title << ' | ' << t('slogan')
   else
     #truncate max priotity to website name
     if @website_name
      final_title = "#{@title.truncate(45, :omission => '')} | #{@website_name} | #{base_title}"
     else
       final_title = "#{@title.truncate(45, :omission => '')} | #{base_title}"
     end
     final_title.truncate(60, :omission => '') 
   end
 end  
  
end