# encoding: utf-8

class ImageUploader < CarrierWave::Uploader::Base

  # Include RMagick or MiniMagick support:
  # include CarrierWave::RMagick
  include CarrierWave::MiniMagick

  # Include the Sprockets helpers for Rails 3.1+ asset pipeline compatibility:
  # include Sprockets::Helpers::RailsHelper
  # include Sprockets::Helpers::IsolatedHelper

  # Choose what kind of storage to use for this uploader:
  storage :file
  # storage :fog

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  # Provide a default URL as a default if there hasn't been a file uploaded:
  # def default_url
  #   # For Rails 3.1+ asset pipeline compatibility:
  #   # asset_path("fallback/" + [version_name, "default.png"].compact.join('_'))
  #
  #   "/images/fallback/" + [version_name, "default.png"].compact.join('_')
  # end

  # Process files as they are uploaded:
  # process :scale => [200, 300]
  #
  # def scale(width, height)
  #   # do something
  # end

  # Create different versions of your uploaded files:
  #version :thumb do
  #  process :scale => [100, 100]
  #end

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  def extension_white_list
    %w(jpg jpeg png)
  end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  # def filename
  #   "something.jpg" if original_filename
  # end

  #version :resized do
  #  process :resize_to_fit => [150, 200]
  #end

  version :resized do
    process :execute_resize
  end

#  version :thumb do
#      process :resize_to_fit => [150, 200]
#  end

  version :cropped_thumb do
    process :crop
    process :resize_to_limit => [300, 150]
  end

  version :cropped do
    process :crop
    process :resize_to_item_specifications
  end

#  def extension_white_list
#    %w(jpg jpeg gif png)
#  end

  def crop
    if model.crop_x.present?
      manipulate! do |img|
        x = model.crop_x.clone
        y = model.crop_y.clone
        w = model.crop_w.clone
        h = model.crop_h.clone
        args = w << 'x' << h << '!+' << x << '+' << y
        img.crop args#w.to_s + "x" + h.to_s + "!" + "0" + x.to_s + "0" + y.to_s
        #img.write "/Users/pedro/Desktop/cenas1.jpg"
        #img.crop "50x50!+0+0"#
        img
      end
    end
  end

  def resize_to_item_specifications
    if model.item_id.present?
      item = Item.find(model.item_id)
      manipulate! do |img|
        img.resize item.width.to_s + "x" + item.height.to_s + "!"
        img
      end
    end
  end


  def execute_resize
    #process :resize_to_limit => [900, 900]
    manipulate! do |img|
      width = img[:width]
      height = img[:height]
      #ratio = img[:width].to_f / img[:height]
      if (width > 900) && (width > height)
        img.resize "900x"
      elsif (height > 564) && (height > width)
        img.resize "x900"
    end
      img
    end
  end



end
