class CreateTemplates < ActiveRecord::Migration
  def change
    create_table :templates do |t|
      t.string :name
      t.string :description
      t.string :icon
      t.integer :output
      t.integer :label, :default => 0
      t.integer :ranking_label
      t.integer :crop_type, :default => 0
      t.boolean :public, :default => false

      t.references :template_category
      t.references :website
      t.references :ad
      t.string :key
      t.timestamps
    end
  end
end
