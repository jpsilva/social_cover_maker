class CreateCounters < ActiveRecord::Migration
  def change
    create_table :counters do |t|
      t.integer :week1
      t.integer :week2
      t.integer :week3
      t.integer :week4
      t.integer :month
      t.integer :all
      t.integer :template_id
      t.references :counter_type

      t.timestamps
    end
  end
end
