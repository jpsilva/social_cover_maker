class CreateBlogComments < ActiveRecord::Migration
  def change
    create_table :blog_comments do |t|
      t.string :name
      t.string :email
      t.string :website
      t.string :comment
      t.boolean :approved, :default => false

      t.references :blog_entry
      t.timestamps
    end
    add_index :blog_comments, :blog_entry_id
  end
end
