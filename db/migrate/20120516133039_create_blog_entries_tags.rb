class CreateBlogEntriesTags < ActiveRecord::Migration
  def change
    create_table :blog_entries_tags do |t|
      t.references :blog_entry
      t.references :tag

      t.timestamps
    end
    add_index :blog_entries_tags, :blog_entry_id
    add_index :blog_entries_tags, :tag_id
  end
end
