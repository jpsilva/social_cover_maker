class CreateBlogEntries < ActiveRecord::Migration
  def change
    create_table :blog_entries do |t|
      t.string :title
      t.text :html
      t.text :excerpt
      t.string :slug

      t.references :blog_category
      t.timestamps
    end
  end
end
