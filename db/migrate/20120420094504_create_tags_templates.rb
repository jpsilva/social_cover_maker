class CreateTagsTemplates < ActiveRecord::Migration
  def change
    create_table :tags_templates do |t|
      t.integer :tag_id
      t.integer :template_id

      t.timestamps
    end
  end
end
