class CreateTemplateComments < ActiveRecord::Migration
  def change
    create_table :template_comments do |t|
      t.string :name
      t.string :email
      t.string :website
      t.string :comment
      t.boolean :approved

      t.references :template
      t.timestamps
    end
    add_index :template_comments, :template_id
  end
end
