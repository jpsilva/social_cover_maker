class CreateImageProcessingJobs < ActiveRecord::Migration
  def change
    create_table :image_processing_jobs do |t|
      t.boolean :finished, :default => false
      t.boolean :success, :default => false
      t.string :session_id, :null => false
      t.references :template, :null => false
      t.timestamps
    end
  end
end
