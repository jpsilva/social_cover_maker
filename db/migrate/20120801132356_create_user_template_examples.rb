class CreateUserTemplateExamples < ActiveRecord::Migration
  def change
    create_table :user_template_examples do |t|
      t.string :email
      t.string :link

      t.references :template
      t.timestamps
    end
  end
end
