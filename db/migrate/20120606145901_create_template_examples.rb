class CreateTemplateExamples < ActiveRecord::Migration
  def change
    create_table :template_examples do |t|
      t.string :main
      t.string :thumb

      t.references :template
      t.timestamps
    end
  end
end
