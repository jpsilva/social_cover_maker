class CreateCounterTypes < ActiveRecord::Migration
  def change
    create_table :counter_types do |t|
      t.string :name

      t.timestamps
    end
  end
end
