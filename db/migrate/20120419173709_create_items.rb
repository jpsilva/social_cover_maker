class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :description
      t.float :size
      t.integer :width
      t.integer :height
      t.boolean :mandatory

      t.references :item_type
      t.references :template
      t.timestamps
    end
  end
end
