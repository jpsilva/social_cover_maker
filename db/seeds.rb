# -*- coding: utf-8 -*-
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

#lib to allow the creation of templates slug
require 'Utils'

# Admin
AdminUser.create!(:email => 'admin@socialcovermaker.com', :password => ';v6,S2t75tqF5N>', :password_confirmation => ';v6,S2t75tqF5N>')

TemplateCategory.create([{name: 'Antique', description: 'Themes with antique stile'},
                 {name: 'Mixed', description: 'Themes with cover and profile photos combined'},
                 {name: 'composed_cover', description: 'Themes with cover composed of many photos'},
                 {name: 'simple_effects', description: 'Themes with simple effects'},
                 {name: 'sublime_effects', description: 'Themes with amazing effects'}])

BlogCategory.create([{name: 'Week Profile', description: 'O perfil mais visitado da semana'},
                 {name: 'Themes', description: 'Posts sobre novos templates'},
                 {name: 'Staff', description: 'Posts do staff a todos os visitantes.'}])

BlogEntry.create([{blog_category_id: 3, html: '<h2>Welcome to Social Cover Maker</h2><p> Social Cover Maker is an easy way to improve your personal page of your favourite Social Network. We are three friends weary of the appearance of google + personal pages, and decided to give a new look to google + profiles. Be free to send us your feedback, we are  only starting and your opinion will be appreciated. Stay close, in the next days we will launch new features and much more... </p>', slug: 'hello_World', title: 'Welcome', excerpt: 'Social Cover Maker is an easy way to improve your personal page of your favourite Social Network. We are three friends weary of the'}])

=begin BlogComment.create([{name: 'Daniel Oliveira', email: 'danyy@mail.com', website: 'www.danieloliveira.pt', comment: 'Concodo com o primeiro parágrafo mas não concordo com o segundo', blog_entry_id: 1, approved: true },
                    {name: 'Cláudio Novais', email: 'claudio@mail.com', website: 'www.claudio.pt', comment: 'Concodo plenamente com a ideia e acho bastante interessante.', blog_entry_id: 1, approved: true},
                    {name: 'Cláudio Novais', email: 'claudio@mail.com', website: 'www.claudio.pt', comment: 'Concodo plenamente com a ideia e acho bastante interessante.', blog_entry_id: 1, approved: true},
                    {name: 'Cláudio Novais', email: 'claudio@mail.com', website: 'www.claudio.pt', comment: 'Concodo plenamente com a ideia e acho bastante interessante.', blog_entry_id: 1, approved: true},
                    {name: 'Cláudio Novais', email: 'claudio@mail.com', website: 'www.claudio.pt', comment: 'Concodo plenamente com a ideia e acho bastante interessante.', blog_entry_id: 1, approved: true},
                    {name: 'Cláudio Novais', email: 'claudio@mail.com', website: 'www.claudio.pt', comment: 'Concodo plenamente com a ideia e acho bastante interessante.', blog_entry_id: 1, approved: true},
                    {name: 'Cláudio Novais', email: 'claudio@mail.com', website: 'www.claudio.pt', comment: 'Concodo plenamente com a ideia e acho bastante interessante.', blog_entry_id: 1, approved: true},
                    {name: 'Cláudio Novais', email: 'claudio@mail.com', website: 'www.claudio.pt', comment: 'Concodo plenamente com a ideia e acho bastante interessante.', blog_entry_id: 1, approved: true},
                    {name: 'Cláudio Novais', email: 'claudio@mail.com', website: 'www.claudio.pt', comment: 'Concodo plenamente com a ideia e acho bastante interessante.', blog_entry_id: 1, approved: true},
                    {name: 'Cláudio Novais', email: 'claudio@mail.com', website: 'www.claudio.pt', comment: 'Concodo plenamente com a ideia e acho bastante interessante.', blog_entry_id: 1, approved: true},
                    {name: 'Cláudio Novais', email: 'claudio@mail.com', website: 'www.claudio.pt', comment: 'Concodo plenamente com a ideia e acho bastante interessante.', blog_entry_id: 1, approved: true},
                    {name: 'Pedro Silva', email: 'pedro@mail.com', website: 'www.pedro.pt', comment: 'Não gostei da ideia, penso que pode ser explorado de um forma diferente', blog_entry_id: 2, approved: true},
                    {name: 'Tiago', email: 'tiago@mail.com', website: 'www.tiago.pt', comment: 'Este é um dos motivos porque gosto de vir aqui.', blog_entry_id: 2, approved: true},
                    {name: 'José', email: 'jose@mail.com', website: 'www.jose.pt', comment: 'Um tema da selacção portuguesa agora para o Euro 2012 seria mais interessante', blog_entry_id: 1, approved: true},
                    {name: 'Passos coelho', email: 'passos@mail.com', website: 'www.passos.pt', comment: 'Curti bue', blog_entry_id: 1, approved: true },
                    {name: 'Zalazar', email: 'Zalazar@mail.com', website: 'www.Zalazar.pt', comment: 'Vou fazer ddos.', blog_entry_id: 1, approved: true}])

=end

Website.create([{name: 'Google+', description: 'Google plus'},
                {name: 'Facebook', description: 'Destinado à rede social facebook'},
                {name: 'Twitter', description: 'Destinado à rede social twitter'}])


=begin t1 = Template.create(name: 'Template 1', description: 'Template relacionado com FC Porto', icon: 'IconTemplate1.jpg', template_category_id: 1, website_id: 1, output: 1)
t2 = Template.create(name: 'Template 2', description: 'Template relacionado com Messi', icon: 'IconTemplate2.jpg', template_category_id: 1, website_id: 1, output: 1)
t3 = Template.create(name: 'Template 3', description: 'Template relacionado com CR7', icon: 'IconTemplate3.jpg', template_category_id: 1, website_id: 1, output: 1)
t4 = Template.create(name: 'Template 4', description: 'Template relacionado com Soraia Chaves', icon: 'IconTemplate4.jpg', template_category_id: 2, website_id: 1, output: 1)
t5 = Template.create(name: 'Template 5', description: 'Template relacionado com Diana Chaves', icon: 'IconTemplate5.jpg', template_category_id: 2, website_id: 1, output: 1)
t6 = Template.create(name: 'Template 6', description: 'Template relacionado com Laura Figueiredo', icon: 'IconTemplate6.jpg', template_category_id: 2, website_id: 1, output: 1)
=end
t1 = Template.create(name: 'Cover and Profile Mixed Photo', description: 'Gera duas images a partir de uma', icon: 'IconTemplate7.jpg', template_category_id: 2, website_id: 1, output: 2, crop_type: 1, key: 'template-7', public: true)
t2 = Template.create(name: 'Cover with Four Photos and Text', description: 'Capa do perfil com 4 imagens submetidase 1 campo de texto', icon: 'IconTemplate8.jpg', template_category_id: 3, website_id: 1, output: 1, key: 'template-8', public: false)
t3 = Template.create(name: 'Cover with Four Photos', description: 'Capa do perfil com 4 imagens submetidas', icon: 'IconTemplate9.jpg', template_category_id: 3, website_id: 1, output: 1, key: 'template-9', public: false)
t4 = Template.create(name: 'Cover with Four Photos and Smooth Background', description: 'Capa do perfil com 4 imagens submetidas e um fundo às riscas', icon: 'IconTemplate10.jpg', template_category_id: 3, website_id: 1, output: 1, key: 'template-10', public: false)
t5 = Template.create(name: 'Cover with Four Photos and striped Background', description: 'Template relacionado com Scorpions', icon: 'IconTemplate11.jpg', template_category_id: 3, website_id: 1, output: 1, key: 'template-11', public: false)
t6 = Template.create(name: 'Cover with Four Photos and Shadows', description: 'Template relacionado com U2', icon: 'IconTemplate12.jpg', template_category_id: 3, website_id: 1, output: 1, key: 'template-12', public: true)
t7 = Template.create(name: 'Cover and Profile Photos look like stamps', description: 'Template recebe 4 imagens mais uma. As primeiras 4 são para a cover e a última é para o perfil. As quatro imagens ficam todas dentro de uma borda de celos dos correios.', icon: 'IconTemplate13.jpg', template_category_id: 1, website_id: 1, output: 2, key: 'template-13', public: false)
t8 = Template.create(name: 'Cover and Profile Photos look like stamps with Profile Background', description: 'Praticamente igual ao 13, apenas difere na imagem do Profile em que tem a continuação da parte castanha. O Template recebe 4 imagens mais uma. As primeiras 4 são para a cover e a última é para o perfil', icon: 'IconTemplate14.jpg', template_category_id: 1, website_id: 1, output: 2, key: 'template-14', public: true)
#t15 = Template.create(name: 'Template 15', description: 'Template relacionado com FC Porto', icon: 'IconTemplate1.jpg', template_category_id: 1, website_id: 1, output: 1, key: 'template-15')
t9 = Template.create(name: 'Cover and Profile Photos Inside Torn Paper', description: 'Template com película de papel', icon: 'IconTemplate1.jpg', template_category_id: 5, website_id: 1, output: 2, crop_type: 1, key: 'template-27', public: true)
t10 = Template.create(name: 'Cover Photo Inside Paper Hole', description: 'Template com película a simular um buraco no papel', icon: 'IconTemplate1.jpg', template_category_id: 5, website_id: 1, output: 1, key: 'template-17', public: false)
t11 = Template.create(name: 'Cover and Profile Photos look like Old Photos', description: 'Template com background a dar um aspecto antigo', icon: 'IconTemplate1.jpg', template_category_id: 1, website_id: 1, output: 2, key: 'template-18', public: true)
#t19 = nao existe
t12 = Template.create(name: 'Cover and Profile Photos Dissolving', description: 'Gera duas images a partir de uma', icon: 'IconTemplate7.jpg', template_category_id: 2, website_id: 1, output: 2, crop_type: 1, key: 'template-20', public: true)
t13 = Template.create(name: 'Cover and Profile Photos on Hanger', description: 'Gera duas images a partir de uma', icon: 'IconTemplate7.jpg', template_category_id: 5, website_id: 1, output: 2, key: 'template-21', public: true)
t14 = Template.create(name: 'Cover and Profile Photos on Dissolving Oval Hole', description: 'Gera duas images a partir de uma', icon: 'IconTemplate7.jpg', template_category_id: 4, website_id: 1, output: 2, key: 'template-22', public: true)
t15 = Template.create(name: 'Cover and Profile Photos on Dissolving Square Hole', description: 'Gera duas images a partir de uma', icon: 'IconTemplate7.jpg', template_category_id: 4, website_id: 1, output: 2, key: 'template-23', public: true)
t16 = Template.create(name: 'Cover and Profile Photos on Dissolving Small Square Hole', description: 'Gera duas images a partir de uma', icon: 'IconTemplate7.jpg', template_category_id: 4, website_id: 1, output: 2, key: 'template-24', public: false)
t17 = Template.create(name: 'Cover and Profile Photos on Dissolving Square Hole', description: 'Gera duas images a partir de uma', icon: 'IconTemplate7.jpg', template_category_id: 4, website_id: 1, output: 2, key: 'template-25', public: false)
t18 = Template.create(name: 'Cover and Profile Photos on Square Hole', description: 'Gera duas images a partir de uma', icon: 'IconTemplate7.jpg', template_category_id: 4, website_id: 1, output: 2, key: 'template-26', public: false)
t19 = Template.create(name: 'Cover and Profile Mixed Photo', description: 'Gera duas images a partir de uma', icon: 'IconTemplate7.jpg', template_category_id: 2, website_id: 2, output: 2, crop_type: 1, key: 'template-28', public: true)
t20 = Template.create(name: 'Cover and Profile Photos Dissolving', description: 'Gera uma imagem que se dissolve', icon: 'IconTemplate7.jpg', template_category_id: 2, website_id: 2, output: 2, crop_type: 1, key: 'template-29', public: true)
t21 = Template.create(name: 'Cover and Profile Photos in Depth', description: 'é a mesma coisa so que com profundidade', icon: 'IconTemplate7.jpg', template_category_id: 2, website_id: 2, output: 2, crop_type: 1, key: 'template-30', public: false)
t22 = Template.create(name: 'Cover with a Hand Holding a Picture', description: 'cover com mão a segurar foto', icon: 'IconTemplate7.jpg', template_category_id: 3, website_id: 2, output: 2, key: 'template-31', public: true)
t23 = Template.create(name: 'Cover with Film Stock', description: 'cover com pelicula de filme', icon: 'IconTemplate7.jpg', template_category_id: 3, website_id: 2, output: 2, key: 'template-32', public: true)
t24 = Template.create(name: 'Cover that resembles a cork board', description: 'Cork board with pictures and message', icon: 'IconTemplate7.jpg', template_category_id: 3, website_id: 2, output: 1, key: 'template-33', public: true)
t25 = Template.create(name: 'Cover with 8 pictures', description: 'Cover with 8 pictures', icon: 'IconTemplate7.jpg', template_category_id: 3, website_id: 2, output: 1, key: 'template-34', public: true)

=begin t1.template_examples.create(:main => 'template9Example.jpg', :thumb => 'Mtemplate9Example.jpg')
t2.template_examples.create(:main => 'template9Example.jpg', :thumb => 'Mtemplate9Example.jpg')
t3.template_examples.create(:main => 'template9Example.jpg', :thumb => 'Mtemplate9Example.jpg')
t4.template_examples.create(:main => 'template9Example.jpg', :thumb => 'Mtemplate9Example.jpg')
t5.template_examples.create(:main => 'template9Example.jpg', :thumb => 'Mtemplate9Example.jpg')
t6.template_examples.create(:main => 'template9Example.jpg', :thumb => 'Mtemplate9Example.jpg')
=end
t1.template_examples.create(:main => 'template7Example.jpg', :thumb => 'Mtemplate7Example.jpg')
t1.template_examples.create(:main => 'template7Example2.jpg', :thumb => 'Mtemplate7Example.jpg')
t2.template_examples.create(:main => 'template8Example.jpg', :thumb => 'Mtemplate8Example.jpg')
t3.template_examples.create(:main => 'template9Example.jpg', :thumb => 'Mtemplate9Example.jpg')
t4.template_examples.create(:main => 'template10Example.jpg', :thumb => 'Mtemplate10Example.jpg')
t5.template_examples.create(:main => 'template11Example.jpg', :thumb => 'Mtemplate11Example.jpg')
t6.template_examples.create(:main => 'template12Example.jpg', :thumb => 'Mtemplate12Example.jpg')
t7.template_examples.create(:main => 'template13Example.jpg', :thumb => 'Mtemplate13Example.jpg')
t8.template_examples.create(:main => 'template14Example.jpg', :thumb => 'Mtemplate14Example.jpg')
#t15.template_examples.create(:main => 'template9Example.jpg', :thumb => 'Mtemplate9Example.jpg')
t9.template_examples.create(:main => 'template27Example.jpg', :thumb => 'Mtemplate27Example.jpg')
t9.template_examples.create(:main => 'template27Example2.jpg', :thumb => 'Mtemplate27Example2.jpg')
t10.template_examples.create(:main => 'template17Example.jpg', :thumb => 'Mtemplate17Example.jpg')
t11.template_examples.create(:main => 'template18Example.jpg', :thumb => 'Mtemplate18Example.jpg')
t12.template_examples.create(:main => 'template20Example.jpg', :thumb => 'Mtemplate20Example.jpg')
t13.template_examples.create(:main => 'template21Example.jpg', :thumb => 'Mtemplate21Example.jpg')
t14.template_examples.create(:main => 'template22Example.jpg', :thumb => 'Mtemplate22Example.jpg')
t15.template_examples.create(:main => 'template23Example.jpg', :thumb => 'Mtemplate23Example.jpg')
t16.template_examples.create(:main => 'template21Example.jpg', :thumb => 'Mtemplate21Example.jpg')
t17.template_examples.create(:main => 'template21Example.jpg', :thumb => 'Mtemplate21Example.jpg')
t18.template_examples.create(:main => 'template21Example.jpg', :thumb => 'Mtemplate21Example.jpg')
t19.template_examples.create(:main => 'template28Example.jpg', :thumb => 'Mtemplate28Example.jpg')
t20.template_examples.create(:main => 'template29Example.jpg', :thumb => 'Mtemplate29Example.jpg')
t21.template_examples.create(:main => 'template30Example.jpg', :thumb => 'Mtemplate30Example.jpg')
t22.template_examples.create(:main => 'template31Example.jpg', :thumb => 'Mtemplate31Example.jpg')
t23.template_examples.create(:main => 'template32Example.jpg', :thumb => 'Mtemplate32Example.jpg')
t24.template_examples.create(:main => 'template33Example.jpg', :thumb => 'Mtemplate33Example.jpg')
t25.template_examples.create(:main => 'template34Example.jpg', :thumb => 'Mtemplate34Example.jpg')

t3.template_comments.create(name: 'João beterraba', email: 'joaob@mail.com', website: 'www.joaobeterraba.pt', comment: 'Gosto muito do T9', approved: true )
t3.template_comments.create(name: 'Manuel Joaquim', email: 'mj@mail.com', website: 'www.manjoaq.pt', comment: 'Desgosto do T9', approved: true )

Tag.create([{name: 'antique'},
            {name: 'hole'},
            {name: 'paper'},
            {name: 'dissolving'},
            {name: 'square'},
            {name: 'stamp'},
            {name: 'combined'},
            {name: 'mixed'},
            {name: 'composed'},
            {name: 'stripe'},
            {name: 'hanger'},
            {name: 'oval'},
            {name: 'old'}])

BlogEntry.find(1).tags.create(name:"hello world")

TagsTemplate.create([{tag_id: 1, template_id: 7},
                      {tag_id: 1, template_id: 8},
                      {tag_id: 1, template_id: 11},
                      {tag_id: 1, template_id: 23},
                      {tag_id: 2, template_id: 10},
                      {tag_id: 2, template_id: 14},
                      {tag_id: 2, template_id: 15},
                      {tag_id: 2, template_id: 16},
                      {tag_id: 2, template_id: 17},
                      {tag_id: 2, template_id: 18},
                      {tag_id: 3, template_id: 9},
                      {tag_id: 3, template_id: 10},
                      {tag_id: 3, template_id: 24},
                      {tag_id: 4, template_id: 12},
                      {tag_id: 4, template_id: 14},
                      {tag_id: 4, template_id: 15},
                      {tag_id: 4, template_id: 16},
                      {tag_id: 4, template_id: 17},
                      {tag_id: 4, template_id: 20},
                      {tag_id: 5, template_id: 15},
                      {tag_id: 5, template_id: 16},
                      {tag_id: 5, template_id: 17},
                      {tag_id: 5, template_id: 18},
                      {tag_id: 5, template_id: 25},
                      {tag_id: 6, template_id: 7},
                      {tag_id: 6, template_id: 8},
                      {tag_id: 7, template_id: 1},
                      {tag_id: 7, template_id: 19},
                      {tag_id: 7, template_id: 12},
                      {tag_id: 7, template_id: 9},
                      {tag_id: 7, template_id: 23},
                      {tag_id: 8, template_id: 1},
                      {tag_id: 8, template_id: 19},
                      {tag_id: 8, template_id: 12},
                      {tag_id: 8, template_id: 9},
                      {tag_id: 8, template_id: 20},
                      {tag_id: 8, template_id: 21},
                      {tag_id: 9, template_id: 5},
                      {tag_id: 9, template_id: 6},
                      {tag_id: 9, template_id: 7},
                      {tag_id: 9, template_id: 8},
                      {tag_id: 9, template_id: 11},
                      {tag_id: 9, template_id: 22},
                      {tag_id: 9, template_id: 24},
                      {tag_id: 9, template_id: 25},
                      {tag_id: 11, template_id: 13},
                      {tag_id: 12, template_id: 14},
                      {tag_id: 13, template_id: 7},
                      {tag_id: 13, template_id: 7},
                      {tag_id: 13, template_id: 8},
                      {tag_id: 13, template_id: 11}])

ItemType.create([{name: 'Imagem', description: 'Item do tipo imagem'},
                  {name: 'Texto', description: 'Item do tipo texto'},
                  {name: 'Icon', description: 'Item do tipo icon'}])

Item.create([{description: 'Imagem que vai ser transformada em 2', width: 940, height: 250, size: 3.76, item_type_id: 1, template_id: 1},
             {description: 'Imagem 1 da esquerda para a direita', width: 170, height: 170, size: 1, item_type_id: 1, template_id: 3},
             {description: 'Imagem 2 da esquerda para a direita', width: 131, height: 131, size: 1, item_type_id: 1, template_id: 3},
             {description: 'Imagem 3 da esquerda para a direita', width: 131, height: 131, size: 1, item_type_id: 1, template_id: 3},
             {description: 'Imagem 4 da esquerda para a direita', width: 131, height: 131, size: 1, item_type_id: 1, template_id: 3},
             {description: 'Imagem 1 da esquerda para a direita', width: 170, height: 170, size: 1, item_type_id: 1, template_id: 2},
             {description: 'Imagem 2 da esquerda para a direita', width: 131, height: 131, size: 1, item_type_id: 1, template_id: 2},
             {description: 'Imagem 3 da esquerda para a direita', width: 131, height: 131, size: 1, item_type_id: 1, template_id: 2},
             {description: 'Imagem 4 da esquerda para a direita', width: 131, height: 131, size: 1, item_type_id: 1, template_id: 2},
             {description: 'Texto',                               size: 10, item_type_id: 2, template_id: 2},
             {description: 'Imagem 1 da esquerda para a direita', width: 170, height: 170, size: 1, item_type_id: 1, template_id: 4},
             {description: 'Imagem 2 da esquerda para a direita', width: 131, height: 131, size: 1, item_type_id: 1, template_id: 4},
             {description: 'Imagem 3 da esquerda para a direita', width: 131, height: 131, size: 1, item_type_id: 1, template_id: 4},
             {description: 'Imagem 4 da esquerda para a direita', width: 131, height: 131, size: 1, item_type_id: 1, template_id: 4},
             {description: 'Imagem 1 da esquerda para a direita', width: 170, height: 170, size: 1, item_type_id: 1, template_id: 5},
             {description: 'Imagem 2 da esquerda para a direita', width: 131, height: 131, size: 1, item_type_id: 1, template_id: 5},
             {description: 'Imagem 3 da esquerda para a direita', width: 131, height: 131, size: 1, item_type_id: 1, template_id: 5},
             {description: 'Imagem 4 da esquerda para a direita', width: 131, height: 131, size: 1, item_type_id: 1, template_id: 5},
             {description: 'Imagem 1 da esquerda para a direita', width: 170, height: 170, size: 1, item_type_id: 1, template_id: 6},
             {description: 'Imagem 2 da esquerda para a direita', width: 131, height: 131, size: 1, item_type_id: 1, template_id: 6},
             {description: 'Imagem 3 da esquerda para a direita', width: 131, height: 131, size: 1, item_type_id: 1, template_id: 6},
             {description: 'Imagem 4 da esquerda para a direita', width: 131, height: 131, size: 1, item_type_id: 1, template_id: 6},
             {description: 'Imagem 1 da esquerda para a direita', width: 170, height: 147, size: 1.156462585, item_type_id: 1, template_id: 7},
             {description: 'Imagem 2 da esquerda para a direita', width: 132, height: 159, size:  0.830188679, item_type_id: 1, template_id: 7},
             {description: 'Imagem 3 da esquerda para a direita', width: 125, height: 121, size: 1.033057851, item_type_id: 1, template_id: 7},
             {description: 'Imagem 4 da esquerda para a direita', width: 123, height: 151, size: 0.814569536, item_type_id: 1, template_id: 7},
             {description: 'Imagem de perfil', width: 250, height: 250, size: 1, item_type_id: 1, template_id: 7},
             {description: 'Imagem 1 da esquerda para a direita', width: 170, height: 147, size: 1.156462585, item_type_id: 1, template_id: 8},
             {description: 'Imagem 2 da esquerda para a direita', width: 132, height: 159, size:  0.830188679, item_type_id: 1, template_id: 8},
             {description: 'Imagem 3 da esquerda para a direita', width: 125, height: 121, size: 1.033057851, item_type_id: 1, template_id: 8},
             {description: 'Imagem 4 da esquerda para a direita', width: 123, height: 151, size: 0.814569536, item_type_id: 1, template_id: 8},
             {description: 'Imagem de perfil', width: 250, height: 250, size: 1, item_type_id: 1, template_id: 8},
             {description: 'Imagem da cover', width: 940, height: 250, size: 3.76, item_type_id: 1, template_id: 9},
             {description: 'Imagem da cover', width: 491, height: 139, size: 3.53237, item_type_id: 1, template_id: 10},
             {description: 'Imagem 1 da esquerda para a direita', width: 147, height: 112, size: 1.3125, item_type_id: 1, template_id: 11},
             {description: 'Imagem 2 da esquerda para a direita', width: 144, height: 108, size:  1.33333, item_type_id: 1, template_id: 11},
             {description: 'Imagem 3 da esquerda para a direita', width: 140, height: 121, size: 1.15702, item_type_id: 1, template_id: 11},
             {description: 'Imagem de Perfil', width: 250, height: 203, size: 1.231527, item_type_id: 1, template_id: 11},
             {description: 'Imagem que vai ser transformada em 2', width: 940, height: 250, size: 3.76, item_type_id: 1, template_id: 12},
             {description: 'Imagem 1 da esquerda para a direita', width: 115, height: 114, size: 1.00877, item_type_id: 1, template_id: 13},
             {description: 'Imagem 2 da esquerda para a direita', width: 84, height: 84, size:  1, item_type_id: 1, template_id: 13},
             {description: 'Imagem 3 da esquerda para a direita', width: 87, height: 83, size: 1.048192, item_type_id: 1, template_id: 13},
             {description: 'Imagem 4 da esquerda para a direita', width: 87, height: 83, size: 1.048192, item_type_id: 1, template_id: 13},
             {description: 'Imagem de perfil', width: 154, height: 138, size: 1.115942, item_type_id: 1, template_id: 13},
             {description: 'Imagem da cover', width: 591, height: 175, size: 3.3771428, item_type_id: 1, template_id: 14},
             {description: 'Imagem de perfil', width: 243, height: 244, size: 0.995901, item_type_id: 1, template_id: 14},
             {description: 'Imagem da cover', width: 615, height: 174, size: 3.53448276, item_type_id: 1, template_id: 15},
             {description: 'Imagem de perfil', width: 240, height: 243, size: 0.987654, item_type_id: 1, template_id: 15},
             {description: 'Imagem da cover', width: 620, height: 178, size: 3.483146, item_type_id: 1, template_id: 16},
             {description: 'Imagem de perfil', width: 241, height: 248, size: 0.971774, item_type_id: 1, template_id: 16},
             {description: 'Imagem da cover', width: 613, height: 178, size: 3.443820, item_type_id: 1, template_id: 17},
             {description: 'Imagem de perfil', width: 241, height: 249, size: 0.96787, item_type_id: 1, template_id: 17},
             {description: 'Imagem da cover', width: 625, height: 180, size: 3.472222, item_type_id: 1, template_id: 18},
             {description: 'Imagem de perfil', width: 241, height: 249, size: 0.96787, item_type_id: 1, template_id: 18},
             {description: 'Imagem que vai ser transformada em 2', width: 851, height: 358, size: 2.38, item_type_id: 1, template_id: 19},
             {description: 'Imagem que vai ser transformada em 2', width: 851, height: 358, size: 2.38, item_type_id: 1, template_id: 20},
             {description: 'Imagem que vai ser transformada em 2', width: 851, height: 358, size: 2.38, item_type_id: 1, template_id: 21},
             {description: 'Imagem para cover', width: 851, height: 358, size: 2.38, item_type_id: 1, template_id: 22},
             {description: 'Imagem pequena por cima da cover', width: 161, height: 108, size: 1.49, item_type_id: 1, template_id: 22},
             {description: 'Imagem para profile', width: 180, height: 180, size: 1, item_type_id: 1, template_id: 22},
             {description: 'Imagem para o quadro', width: 475, height: 273, size: 1.74, item_type_id: 1, template_id: 23},
             {description: 'Imagem para a película de filme', width: 169, height: 162, size: 1.04, item_type_id: 1, template_id: 23},
             {description: 'Imagem para profile', width: 180, height: 180, size: 1, item_type_id: 1, template_id: 23},
             {description: 'Imagem 1 para cover', width: 121, height: 161, size: 0.75, item_type_id: 1, template_id: 24},
             {description: 'Imagem 2 para cover', width: 159, height: 121, size: 1.31, item_type_id: 1, template_id: 24},
             {description: 'Imagem 3 para cover', width: 161, height: 121, size: 1.33, item_type_id: 1, template_id: 24},
             {description: 'Texto 1 para cover', width: 1, height: 1, size: 140, item_type_id: 2, template_id: 24},
             {description: 'Imagem1', width: 180, height: 294, size: 0.61, item_type_id: 1, template_id: 25},
             {description: 'Imagem2', width: 296, height: 140, size: 2.11, item_type_id: 1, template_id: 25},
             {description: 'Imagem3', width: 140, height: 140, size: 1, item_type_id: 1, template_id: 25},
             {description: 'Imagem4', width: 140, height: 140, size: 1, item_type_id: 1, template_id: 25},
             {description: 'Imagem5', width: 168, height: 294, size: 0.57, item_type_id: 1, template_id: 25},
             {description: 'Imagem6', width: 140, height: 140, size: 1, item_type_id: 1, template_id: 25},
             {description: 'Imagem7', width: 140, height: 140, size: 1, item_type_id: 1, template_id: 25}
             ])


CounterType.create([{name: 'Executions'},
                    {name: 'Views'}])




